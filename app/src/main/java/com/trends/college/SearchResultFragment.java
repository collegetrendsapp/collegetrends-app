package com.trends.college;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.idik.lib.slimadapter.SlimAdapter;
import net.idik.lib.slimadapter.SlimInjector;
import net.idik.lib.slimadapter.viewinjector.IViewInjector;

public class SearchResultFragment extends Fragment {
    private SlimAdapter slimAdapter;
    private RecyclerView recyclerView;
    private FragmentActivity parentActivity;

    public SearchResultFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        parentActivity = getActivity();
        //verifyToken();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_search_result, container, false);
        Toolbar toolbar = v.findViewById(R.id.toolbar2);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(v1 -> {
            NavigationActivity.currentCommunityName = NavigationActivity.communityNavName.pop();
                NavigationActivity.currentCommunityID = NavigationActivity.communityNavID.pop();
                parentActivity.getSupportFragmentManager()
                        .beginTransaction().replace(R.id.content_frame, new ListFragment()).commit();
        });
        recyclerView = v.findViewById(R.id.recycler);
        LinearLayoutManager linearLayoutManagerComm = new LinearLayoutManager(getActivity());
        linearLayoutManagerComm.canScrollVertically();
        recyclerView.setLayoutManager(linearLayoutManagerComm);
        initSlimAdapter();
        return v;
    }

    private void initSlimAdapter() {
        slimAdapter = SlimAdapter.create()
                .register(R.layout.layout_community, new SlimInjector<Community>() {
                    @Override
                    public void onInject(@NonNull final Community data, @NonNull IViewInjector injector) {
                        injector.text(R.id.commName, data.getName())
                                .clicked(R.id.commName, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        NavigationActivity.communityNavID.push(NavigationActivity.currentCommunityID);
                                        NavigationActivity.communityNavName.push(NavigationActivity.currentCommunityName);
                                        NavigationActivity.currentCommunityID = data.getId();
                                        NavigationActivity.currentCommunityName = data.getName();
                                        parentActivity.getSupportFragmentManager()
                                                .beginTransaction().replace(R.id.content_frame, new ListFragment()).commit();
                                        //verifyToken();
                                    }
                                })
                                .clicked(R.id.communityLayout, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        NavigationActivity.communityNavID.push(NavigationActivity.currentCommunityID);
                                        NavigationActivity.communityNavName.push(NavigationActivity.currentCommunityName);
                                        NavigationActivity.currentCommunityID = data.getId();
                                        NavigationActivity.currentCommunityName = data.getName();
                                        parentActivity.getSupportFragmentManager()
                                                .beginTransaction().replace(R.id.content_frame, new ListFragment()).commit();
                                        //verifyToken();

                                    }
                                });
                        //.text(R.id.fareEstimate, data.getEstimate() + "")
                    }
                })
                .register(R.layout.layout_schedule, new SlimInjector<Schedule>() {
                    @Override
                    public void onInject(@NonNull final Schedule data, @NonNull IViewInjector injector) {
                        injector.text(R.id.teachName, data.getFname() + " " + data.getLname())
                                .text(R.id.schedTitle, data.getTitle())
                                .text(R.id.dateTimeSchedule, data.getTimestamp())
                                .clicked(R.id.teachName, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        //do whatever
                                    }
                                })
                                .clicked(R.id.scheduleLayout, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Intent intent = new Intent(getActivity(), ViewScheduleActivity.class);
                                        intent.putExtra("timeStamp", data.getTimestamp());
                                        intent.putExtra("title", data.getTitle());
                                        intent.putExtra("content", data.getContent());
                                        intent.putExtra("thread", data.getThread());
                                        intent.putExtra("link", String.valueOf(data.getLink()));
                                        startActivity(intent);
                                    }
                                });
                    }
                })
                .register(R.layout.layout_notes, new SlimInjector<Notes>() {
                    @Override
                    public void onInject(@NonNull final Notes data, @NonNull IViewInjector injector) {
                        injector.text(R.id.teacherName, data.getFname() + " " + data.getLname())
                                .text(R.id.title, data.getTitle())
                                .text(R.id.dateTimeNotes, data.getTimestamp())
                                .longClicked(R.id.teacherName, new View.OnLongClickListener() {
                                    @Override
                                    public boolean onLongClick(View view) {
                                        //do whatever
                                        return false;
                                    }
                                })
                                .clicked(R.id.notesLayout, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Intent intent = new Intent(getActivity(), ViewMaterialActivity.class);
                                        intent.putExtra("link", String.valueOf(data.getLink()));
                                        intent.putExtra("title", data.getTitle());
                                        intent.putExtra("content", data.getContent());
                                        intent.putExtra("thread", data.getThread());
                                        startActivity(intent);
                                        /*Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(String.valueOf(data.getLink())));
                                        startActivity(browserIntent);*/
                                    }
                                });
                    }
                })
                .enableDiff().attachTo(recyclerView);
        slimAdapter.updateData(NavigationActivity.searchResult);
    }
}