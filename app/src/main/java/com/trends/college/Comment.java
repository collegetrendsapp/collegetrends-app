package com.trends.college;

/**
 * Comment class structure, constructor and getters-setters
 */
class Comment { //basic skeleton class for storing Comment details
    private int id;
    private String level;
    private String content;
    private String fname;
    private String lname;
    private String timestamp;

    /**
     * Constructor for comments
     *
     * @param id        Comment ID
     * @param level     Comment level
     * @param content   Comment text description
     * @param fname     Comment user first name
     * @param lname     Comment user last name
     * @param timestamp Comment made on time
     */
    Comment(int id, String level, String content, String fname, String lname, String timestamp) {
        this.id = id;
        this.content = content;
        this.fname = fname;
        this.lname = lname;
        this.timestamp = timestamp;
        this.level = level;
    }

    /**
     * Comment ID
     *
     * @return Comment IF
     */
    int getId() {
        return id;
    }

    /**
     * Comment Level
     *
     * @return Comment Level
     */
    String getLevel() {
        return level;
    }

    /**
     * Comment content
     *
     * @return Comment content
     */
    String getContent() {
        return content;
    }

    /**
     * Comment user first name
     *
     * @return First name
     */
    String getFname() {
        return fname;
    }

    /**
     * Comment user last name
     *
     * @return Last name
     */
    String getLname() {
        return lname;
    }

    /**
     * Comment creation time
     * @return timestamp
     */
    String getTimestamp() {
        return timestamp;
    }
}