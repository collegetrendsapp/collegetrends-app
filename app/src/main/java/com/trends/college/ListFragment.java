package com.trends.college;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import net.idik.lib.slimadapter.SlimAdapter;
import net.idik.lib.slimadapter.SlimInjector;
import net.idik.lib.slimadapter.viewinjector.IViewInjector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpCookie;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Main fragment attached by default to NavigationActivity
 */
public class ListFragment extends Fragment {
    public static ArrayList<Community> currentComm;
    public static ArrayList<Notes> currentNotes;
    public static ArrayList<Schedule> currentSchedule;
    private SlimAdapter slimAdapterComm;
    private SlimAdapter slimAdapterNotes;
    private SlimAdapter slimAdapterSchedule;
    private FragmentActivity parentActivity;
    private RecyclerView recyclerViewComm;
    private RecyclerView recyclerViewNotes;
    private RecyclerView recyclerViewSchedule;
    private TextView scheduleTitle;
    private TextView materialTitle;
    private TextView communityTitle;
    private View scheduleDivider;
    private View materialDivider;
    private View communityDivider;


    /**
     * Default constructor
     */
    public ListFragment() {

    }

    /**
     * Overriden method to display list of communities, materials and schedules in a nested manner
     *
     * @param inflater           Associated inflater to retrieve activity view
     * @param container          ViewGroup attached to XML
     * @param savedInstanceState Modify to store data persistently on onResume() and onPause() call
     * @return View to activity
     */
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_list, container, false);
        TextView commName = v.findViewById(R.id.commName);
        scheduleTitle=v.findViewById(R.id.scheduleTitle);
        communityTitle=v.findViewById(R.id.communityTitle);
        materialTitle=v.findViewById(R.id.materialTitle);
        scheduleDivider=v.findViewById(R.id.scheduleDivider);
        communityDivider=v.findViewById(R.id.communityDivider);
        materialDivider=v.findViewById(R.id.materialDivider);
        Toolbar toolbar = v.findViewById(R.id.toolbar2);
        if (NavigationActivity.currentCommunityID != -1) {
            toolbar.setNavigationIcon(R.drawable.ic_back);
            getActivity().findViewById(R.id.arcMenu_admin).setVisibility(View.GONE);
            getActivity().findViewById(R.id.arcMenu_faculty).setVisibility(View.GONE);
        }
        if (NavigationActivity.currentCommunityID == -1) {
            getActivity().findViewById(R.id.arcMenu_admin).setVisibility(View.GONE);
            getActivity().findViewById(R.id.arcMenu_faculty).setVisibility(View.GONE);
        } else if (SharedPrefManager.getInstance(getActivity()).checkAccountType() == 1) {
            getActivity().findViewById(R.id.arcMenu_faculty).setVisibility(View.VISIBLE);

        } else if (SharedPrefManager.getInstance(getActivity()).checkAccountType() == 2) {
            getActivity().findViewById(R.id.arcMenu_admin).setVisibility(View.VISIBLE);

        }//if not parent community show this else ignore
        toolbar.setNavigationOnClickListener(v1 -> {
            NavigationActivity.currentCommunityName = NavigationActivity.communityNavName.pop();
            if (NavigationActivity.currentCommunityID != -1) {
                NavigationActivity.currentCommunityID = NavigationActivity.communityNavID.pop();
                parentActivity.getSupportFragmentManager()
                        .beginTransaction().replace(R.id.content_frame, new ListFragment()).commit();
            }
        });
        commName.setText(NavigationActivity.currentCommunityName);
        toolbar.setElevation(0);
        recyclerViewComm = v.findViewById(R.id.recycler_comm);
        LinearLayoutManager linearLayoutManagerComm = new LinearLayoutManager(getActivity());
        linearLayoutManagerComm.canScrollVertically();
        recyclerViewComm.setLayoutManager(linearLayoutManagerComm);
        recyclerViewComm.setNestedScrollingEnabled(false);
        recyclerViewComm.setHasFixedSize(true);
        recyclerViewNotes = v.findViewById(R.id.recycler_notes);
        LinearLayoutManager linearLayoutManagerNotes = new LinearLayoutManager(getActivity());
        linearLayoutManagerNotes.canScrollVertically();
        linearLayoutManagerNotes.setReverseLayout(true);
        recyclerViewNotes.setLayoutManager(linearLayoutManagerNotes);
        recyclerViewNotes.setNestedScrollingEnabled(false);
        recyclerViewNotes.setHasFixedSize(true);
        recyclerViewSchedule = v.findViewById(R.id.recycler_schedule);
        LinearLayoutManager linearLayoutManagerSchedule = new LinearLayoutManager(getActivity());
        linearLayoutManagerSchedule.canScrollVertically();
        linearLayoutManagerSchedule.setReverseLayout(true);
        recyclerViewSchedule.setLayoutManager(linearLayoutManagerSchedule);
        recyclerViewSchedule.setNestedScrollingEnabled(false);
        recyclerViewSchedule.setHasFixedSize(true);
        initSlimAdapter();
        return v;
    }

    /**
     * Initializes SlimAdapter library to populate recycler view
     */
    private void initSlimAdapter() {
        //slim adapter for communities list
        slimAdapterComm = SlimAdapter.create()
                .register(R.layout.layout_community, new SlimInjector<Community>() {
                    @Override
                    public void onInject(@NonNull final Community data, @NonNull IViewInjector injector) {
                        injector.text(R.id.commName, data.getName())
                                .clicked(R.id.commName, view -> {
                                    NavigationActivity.communityNavID.push(NavigationActivity.currentCommunityID);
                                    NavigationActivity.communityNavName.push(NavigationActivity.currentCommunityName);
                                    NavigationActivity.currentCommunityID = data.getId();
                                    NavigationActivity.currentCommunityName = data.getName();
                                    parentActivity.getSupportFragmentManager()
                                            .beginTransaction().replace(R.id.content_frame, new ListFragment()).commit();
                                    //verifyToken();
                                })
                                .clicked(R.id.communityLayout, view -> {
                                    NavigationActivity.communityNavID.push(NavigationActivity.currentCommunityID);
                                    NavigationActivity.communityNavName.push(NavigationActivity.currentCommunityName);
                                    NavigationActivity.currentCommunityID = data.getId();
                                    NavigationActivity.currentCommunityName = data.getName();
                                    parentActivity.getSupportFragmentManager()
                                            .beginTransaction().replace(R.id.content_frame, new ListFragment()).commit();
                                    //verifyToken();

                                });
                        //.text(R.id.fareEstimate, data.getEstimate() + "")
                    }
                }).enableDiff().attachTo(recyclerViewComm);

        //slim adapter for schedules list
        slimAdapterSchedule = SlimAdapter.create()
                .register(R.layout.layout_schedule, new SlimInjector<Schedule>() {
                    @Override
                    public void onInject(@NonNull final Schedule data, @NonNull IViewInjector injector) {
                        injector.text(R.id.teachName, data.getFname() + " " + data.getLname())
                                .text(R.id.schedTitle, data.getTitle())
                                .text(R.id.dateTimeSchedule, data.getTimestamp())
                                .clicked(R.id.teachName, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        //do whatever
                                    }
                                })
                                .clicked(R.id.scheduleLayout, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Intent intent = new Intent(getActivity(), ViewScheduleActivity.class);
                                        intent.putExtra("timeStamp", data.getTimestamp());
                                        intent.putExtra("title", data.getTitle());
                                        intent.putExtra("content", data.getContent());
                                        intent.putExtra("thread", data.getThread());
                                        intent.putExtra("link", String.valueOf(data.getLink()));
                                        startActivity(intent);
                                    }
                                });
                    }
                }).enableDiff().attachTo(recyclerViewSchedule);

        //slim adapter for materials list
        slimAdapterNotes = SlimAdapter.create()
                .register(R.layout.layout_notes, new SlimInjector<Notes>() {
                    @Override
                    public void onInject(@NonNull final Notes data, @NonNull IViewInjector injector) {
                        injector.text(R.id.teacherName, data.getFname() + " " + data.getLname())
                                .text(R.id.title, data.getTitle())
                                .text(R.id.dateTimeNotes, data.getTimestamp())
                                .longClicked(R.id.teacherName, new View.OnLongClickListener() {
                                    @Override
                                    public boolean onLongClick(View view) {
                                        //do whatever
                                        return false;
                                    }
                                })
                                .clicked(R.id.notesLayout, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Intent intent = new Intent(getActivity(), ViewMaterialActivity.class);
                                        intent.putExtra("link", String.valueOf(data.getLink()));
                                        intent.putExtra("title", data.getTitle());
                                        intent.putExtra("content", data.getContent());
                                        intent.putExtra("thread", data.getThread());
                                        startActivity(intent);
                                        /*Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(String.valueOf(data.getLink())));
                                        startActivity(browserIntent);*/
                                    }
                                });
                    }
                })
                .enableDiff().attachTo(recyclerViewNotes);
    }

    /**
     * Overriden method called when fragment loaded
     * @param context Activity context
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        parentActivity = getActivity();
        verifyToken();
    }

    /**
     * HTTP request to get data for list of communities, materials, notes
     * It also creates the UI list for communities, materials, notes
     */
    private void getData() {
        /*if ((!ConnectionManager.isNetworkAvailable(parentActivity)) || (!ConnectionManager.hasInternetAccess(parentActivity)))
            return;*/
        hideKeyboard(parentActivity);
        StringRequest getNotes = new StringRequest(Request.Method.POST, ServerURL.GET_NOTES_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("NotesResponse", response);
                response = response.replaceAll("[\\\\]", "");
                response = response.replace("\"{", "{");
                response = response.replace("}\"", "}");
                try {
                    JSONArray userObj = new JSONArray(response);
                    JSONObject checkSuccess = userObj.getJSONObject(0);
                    if (response != null && checkSuccess.getInt("success") == 1) {
                        materialTitle.setVisibility(View.VISIBLE);
                        materialDivider.setVisibility(View.VISIBLE);
                        userObj = userObj.getJSONArray(1);
                        if(userObj.length()==0){
                            materialTitle.setVisibility(View.GONE);
                            materialDivider.setVisibility(View.GONE);
                        }
                        for (int i = 0; i < userObj.length(); i++) {
                            JSONObject obj = userObj.getJSONObject(i);
                            String fname = obj.getString("faculty_fname");
                            String lname = obj.getString("faculty_lname");
                            Uri link = Uri.parse(obj.getString("link"));
                            String content = obj.getString("content");
                            String thread = obj.getString("thread");
                            String timestamp = obj.getString("timestamp");
                            String title = obj.getString("title");
                            int id = obj.getInt("id");
                            String tags = obj.getString("tags");
                            tags = tags.substring(1, tags.length() - 1);
                            tags = tags.replace('\'', ' ');
                            currentNotes.add(new Notes(id, title, content, fname, lname, thread, timestamp, link, tags.split(",")));
                        }
                        slimAdapterNotes.updateData(currentNotes);
                    }
                    else{
                        materialTitle.setVisibility(View.GONE);
                        materialDivider.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    Log.e("NotesResponse", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("NotesErrorResponse", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                LoginCookies loginCookies = SharedPrefManager.getInstance(parentActivity).getLogin();
                params.put("X-CSRF-Token", loginCookies.getCsrfAccessCookie());
                params.put("Cookie", LoginCookies.JWT_ACCESS_COOKIE_NAME + "=" + loginCookies.getAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_COOKIE_NAME + "=" + loginCookies.getRefreshCookie() +
                        "; " + LoginCookies.JWT_ACCESS_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfRefreshCookie());
                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("community", String.valueOf(NavigationActivity.currentCommunityID));
                return params;
            }
        };
        NavigationActivity.requestQueue.add(getNotes);
        StringRequest getSchedules = new StringRequest(Request.Method.POST, ServerURL.GET_SCHEDULE_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("ScheduleResponse", response);
                response = response.replaceAll("[\\\\]", "");
                response = response.replace("\"{", "{");
                response = response.replace("}\"", "}");
                try {
                    JSONArray userObj = new JSONArray(response);
                    JSONObject checkSuccess = userObj.getJSONObject(0);
                    if (response != null && checkSuccess.getInt("success") == 1) {
                        scheduleTitle.setVisibility(View.VISIBLE);
                        scheduleDivider.setVisibility(View.VISIBLE);
                        userObj = userObj.getJSONArray(1);
                        if(userObj.length()==0){
                            scheduleTitle.setVisibility(View.GONE);
                            scheduleDivider.setVisibility(View.GONE);
                        }
                        for (int i = 0; i < userObj.length(); i++) {
                            JSONObject obj = userObj.getJSONObject(i);
                            String fname = obj.getString("faculty_fname");
                            String lname = obj.getString("faculty_lname");
                            Uri link = Uri.parse(obj.getString("link"));
                            String content = obj.getString("content");
                            String thread = obj.getString("thread");
                            String timestamp = obj.getString("timestamp");
                            String title = obj.getString("title");
                            int id = obj.getInt("id");
                            currentSchedule.add(new Schedule(id, title, content, fname, lname, thread, timestamp, link));
                        }
                        slimAdapterSchedule.updateData(currentSchedule);
                    }
                    else{
                        scheduleTitle.setVisibility(View.GONE);
                        scheduleDivider.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    Log.e("ScheduleResponse", e.toString());
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ScheduleErrorResponse", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                LoginCookies loginCookies = SharedPrefManager.getInstance(parentActivity).getLogin();
                params.put("X-CSRF-Token", loginCookies.getCsrfAccessCookie());
                params.put("Cookie", LoginCookies.JWT_ACCESS_COOKIE_NAME + "=" + loginCookies.getAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_COOKIE_NAME + "=" + loginCookies.getRefreshCookie() +
                        "; " + LoginCookies.JWT_ACCESS_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfRefreshCookie());
                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("community", String.valueOf(NavigationActivity.currentCommunityID));
                return params;
            }
        };
        NavigationActivity.requestQueue.add(getSchedules);
        StringRequest getCommunities = new StringRequest(Request.Method.POST, ServerURL.GET_COMMUNITIES_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("CommResponse", response);
                response = response.replaceAll("[\\\\]", "");
                response = response.replace("\"{", "{");
                response = response.replace("}\"", "}");
                try {
                    JSONArray userObj = new JSONArray(response);
                    JSONObject checkSuccess = userObj.getJSONObject(0);
                    if (response != null && checkSuccess.getInt("success") == 1) {
                        communityTitle.setVisibility(View.VISIBLE);
                        communityDivider.setVisibility(View.VISIBLE);
                        userObj = userObj.getJSONArray(1);
                        if(userObj.length()==0){
                            communityTitle.setVisibility(View.GONE);
                            communityDivider.setVisibility(View.GONE);
                        }
                        for (int i = 0; i < userObj.length(); i++) {
                            JSONObject obj = userObj.getJSONObject(i);
                            String name = obj.getString("name");
                            int id = obj.getInt("id");
                            currentComm.add(new Community(id, name));
                        }
                        slimAdapterComm.updateData(currentComm);
                    }
                    else{
                        communityTitle.setVisibility(View.GONE);
                        communityDivider.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    Log.e("CommResponse", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("CommErrorResponse", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                LoginCookies loginCookies = SharedPrefManager.getInstance(parentActivity).getLogin();
                params.put("X-CSRF-Token", loginCookies.getCsrfAccessCookie());
                params.put("Cookie", LoginCookies.JWT_ACCESS_COOKIE_NAME + "=" + loginCookies.getAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_COOKIE_NAME + "=" + loginCookies.getRefreshCookie() +
                        "; " + LoginCookies.JWT_ACCESS_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfRefreshCookie());
                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("cid", String.valueOf(NavigationActivity.currentCommunityID));
                return params;
            }
        };
        NavigationActivity.requestQueue.add(getCommunities);
    }

    /**
     * Verifies current session cookies
     */
    private void verifyToken() {
        /*if ((!ConnectionManager.isNetworkAvailable(parentActivity)) || (!ConnectionManager.hasInternetAccess(parentActivity)))
            return;*/
        currentComm = new ArrayList<>();
        currentNotes = new ArrayList<>();
        currentSchedule = new ArrayList<>();
        hideKeyboard(parentActivity);
        StringRequest verify = new StringRequest(Request.Method.POST, ServerURL.VERIFY_TOKEN_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject responseObj = new JSONObject(response);
                            if (responseObj.getInt("success") != 1) refreshToken();
                            else getData();
                        } catch (JSONException e) {
                            refreshToken();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Do stuff
                        //Probably means 401 - so try refresh
                        refreshToken();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                LoginCookies loginCookies = SharedPrefManager.getInstance(parentActivity).getLogin();
                params.put("X-CSRF-Token", loginCookies.getCsrfAccessCookie());
                params.put("Cookie", LoginCookies.JWT_ACCESS_COOKIE_NAME + "=" + loginCookies.getAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_COOKIE_NAME + "=" + loginCookies.getRefreshCookie() +
                        "; " + LoginCookies.JWT_ACCESS_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfRefreshCookie());
                return params;
            }
        };

        NavigationActivity.requestQueue.add(verify);
    }

    /**
     * Refresh cookies if expired
     */
    private void refreshToken() {
        final CookieManager manager = new CookieManager(null, CookiePolicy.ACCEPT_ALL);
        CookieHandler.setDefault(manager);
        StringRequest refresh = new StringRequest(Request.Method.POST, ServerURL.REFRESH_TOKEN_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject responseObj = new JSONObject(response);
                            if (!Objects.equals(responseObj.getString("refresh"), "true")) {
                                Toast.makeText(parentActivity, "Please logout and login again", Toast.LENGTH_SHORT).show();
                            } else {
                                List<HttpCookie> cookieList = manager.getCookieStore().getCookies();
                                boolean accessCookieFound = false;
                                boolean accessCsrfFound = false;
                                Map<String, String> cookies = new HashMap<>();
                                for (HttpCookie cookie : cookieList) {
                                    if (cookie.getName().equals(LoginCookies.JWT_ACCESS_COOKIE_NAME)) {
                                        cookies.put(LoginCookies.JWT_ACCESS_COOKIE_NAME, cookie.getValue());
                                        accessCookieFound = true;
                                    }
                                    if (cookie.getName().equals(LoginCookies.JWT_ACCESS_CSRF_COOKIE_NAME)) {
                                        cookies.put(LoginCookies.JWT_ACCESS_CSRF_COOKIE_NAME, cookie.getValue());
                                        accessCsrfFound = true;
                                    }
                                    if (accessCookieFound && accessCsrfFound) {
                                        SharedPrefManager.getInstance(parentActivity).setAccessCookie(cookies);
                                        break;
                                    }
                                }

                                if (accessCookieFound && accessCsrfFound)
                                    getData();
                                else
                                    Toast.makeText(parentActivity, "Please logout and login again", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException | NullPointerException e) {
                            Toast.makeText(parentActivity, e.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Do stuff
                        //this means that your refresh token has expired!
                        Toast.makeText(parentActivity, "Please logout and login again", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                LoginCookies loginCookies = SharedPrefManager.getInstance(parentActivity).getLogin();
                params.put("X-CSRF-Token", loginCookies.getCsrfRefreshCookie());
                params.put("Cookie", LoginCookies.JWT_ACCESS_COOKIE_NAME + "=" + loginCookies.getAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_COOKIE_NAME + "=" + loginCookies.getRefreshCookie() +
                        "; " + LoginCookies.JWT_ACCESS_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfRefreshCookie());
                return params;
            }
        };
        NavigationActivity.requestQueue.add(refresh);
    }

    /**
     * Prevents EditText views from gaining focus at activity startup
     * @param activity Specify current activity view
     */
    void hideKeyboard(Activity activity) {
        InputMethodManager inputMethodManager;
        inputMethodManager = (InputMethodManager) parentActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
        View currentFocusedView = activity.getCurrentFocus();
        if (currentFocusedView != null && inputMethodManager != null)
            inputMethodManager.hideSoftInputFromWindow(currentFocusedView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    /**
     * Called when user clicks on search
     * @param query Search query submitted by user
     */
    private void search(String query) {
        NavigationActivity.searchResult = new ArrayList<>();
        for (int i = 0; i < currentNotes.size(); i++) {
            Notes notes = currentNotes.get(i);
            String tags[] = notes.getTags();
            if (tags.length == 0 && notes.getTitle().equalsIgnoreCase(query))
                NavigationActivity.searchResult.add(notes);
            for (String tag : tags) {
                if (query.equalsIgnoreCase(tag)) {
                    NavigationActivity.searchResult.add(notes);
                    break;
                }
            }
        }
        for (int i = 0; i < currentSchedule.size(); i++) {
            Schedule schedule = currentSchedule.get(i);
            if (schedule.getTitle().equalsIgnoreCase(query))
                NavigationActivity.searchResult.add(schedule);
        }
        if (!NavigationActivity.searchResult.isEmpty())
            parentActivity.getSupportFragmentManager()
                    .beginTransaction().replace(R.id.content_frame, new SearchResultFragment()).commit();
        else Toast.makeText(parentActivity, "No Results", Toast.LENGTH_SHORT).show();
    }
}