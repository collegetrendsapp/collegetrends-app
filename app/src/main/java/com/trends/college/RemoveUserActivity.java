package com.trends.college;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpCookie;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import io.apptik.widget.multiselectspinner.MultiSelectSpinner;

public class RemoveUserActivity extends AppCompatActivity {
    private String users;
    private String usernames;
    static volatile RequestQueue requestQueue;
    private List<String> items;
    private List<String> itemsUserName;
    MultiSelectSpinner multiSelectSpinner;
    private String grantUsers;
    private boolean[] checked;
    private Boolean check;
    private Button remove;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remove_user);
        check=false;
        requestQueue = Volley.newRequestQueue(this);
        multiSelectSpinner = findViewById(R.id.multiselectSpinner);
        remove=findViewById(R.id.remove);
        grantUsers="";
        ImageView back = findViewById(R.id.backButton);
        requestQueue = Volley.newRequestQueue(this);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), NavigationActivity.class));
                finish();
            }
        });
        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(checked.length<1){
                    Toast.makeText(RemoveUserActivity.this, "You need to remove atleast one user", Toast.LENGTH_SHORT).show();
                    return;
                }
                int trueSize=0;
                int size=items.size();
                for(int i=0;i<size;i++){
                    if(checked[i])
                        trueSize+=1;
                }
                for(int i=0,j=0;i<size;i++){
                    if(checked[i]) {
                        if (j < trueSize - 1)
                            grantUsers = grantUsers + items.get(i) + ",";
                        else
                            grantUsers = grantUsers + items.get(i);
                    }
                }
                verifyToken();
            }
        });
        verifyTokenStart();
    }

    private void getUserList() {
        /*if ((!ConnectionManager.isNetworkAvailable(parentActivity)) || (!ConnectionManager.hasInternetAccess(parentActivity)))
            return;*/
        //hideKeyboard(parentActivity);
        StringRequest getUsersList = new StringRequest(Request.Method.POST, ServerURL.LIST_USER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("ListUsersResponse", response);
                try {
                    JSONObject checkSuccess = new JSONObject(response);
                    int success = checkSuccess.getInt("success");
                    if (response != null && success == 1) {
                        users = checkSuccess.getString("users");
                        usernames= checkSuccess.getString("usernames");
                        users=users.substring(1, users.length()-1);
                        items = Arrays.asList(users.split("\\s*,\\s*"));
                        usernames=usernames.substring(1, usernames.length()-1);
                        itemsUserName = Arrays.asList(usernames.split("\\s*,\\s*"));
                        int itemSize=items.size();
                        for(int i=0;i<itemSize;i++){
                            itemsUserName.set(i,itemsUserName.get(i).substring(1, itemsUserName.get(i).length()-1));
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter <>(RemoveUserActivity.this, android.R.layout.simple_list_item_multiple_choice, itemsUserName);
                        checked=new boolean[itemSize];
                        for(int i=0;i<itemSize;i++){
                            checked[i]=false;
                        }
                        multiSelectSpinner
                                .setListAdapter(adapter)
                                .setListener(new MultiSelectSpinner.MultiSpinnerListener() {
                                    @Override
                                    public void onItemsSelected(boolean[] selected) {
                                        checked=selected;
                                    }
                                })
                                .setAllCheckedText("All Students")
                                .setAllUncheckedText("No Student Selected")
                                .setTitle("Select Students")
                                .setMinSelectedItems(1)
                                .setMaxSelectedItems(itemSize-2)
                                .setChoiceDialogTheme(R.style.AlertDialogCustom);
                        multiSelectSpinner.setTitleDividerColor(getResources().getColor(R.color.black));
                        //Toast.makeText(CommunityActivity.this, users, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    Log.e("CommentsResponse", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("CommentsErrorResponse", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                LoginCookies loginCookies = SharedPrefManager.getInstance(RemoveUserActivity.this).getLogin();
                params.put("X-CSRF-Token", loginCookies.getCsrfAccessCookie());
                params.put("Cookie", LoginCookies.JWT_ACCESS_COOKIE_NAME + "=" + loginCookies.getAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_COOKIE_NAME + "=" + loginCookies.getRefreshCookie() +
                        "; " + LoginCookies.JWT_ACCESS_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfRefreshCookie());
                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("community",  String.valueOf(NavigationActivity.currentCommunityID));
                params.put("strict",  "1");
                return params;
            }
        };
        requestQueue.add(getUsersList);
    }
    @Override
    public void onBackPressed() {
        startActivity(new Intent(getApplicationContext(), NavigationActivity.class));
        finish();
    }
    private void getData() {
        if ((!ConnectionManager.isNetworkAvailable(this)) || (!ConnectionManager.hasInternetAccess(this)))
            return;
        StringRequest removeUser = new StringRequest(Request.Method.POST, ServerURL.REMOVE_USER_COMMUNITY, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("RemoveResponse", response);
                try {
                    JSONObject checkSuccess = new JSONObject(response);
                    int success = checkSuccess.getInt("success");
                    if (success == 1) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(RemoveUserActivity.this, R.style.DialogTheme);
                        builder.setTitle("Users Removed")
                                .setMessage("Selected Users are Removed.")
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        check=true;
                                        startActivity(new Intent(getApplicationContext(), NavigationActivity.class));
                                        finish();
                                    }
                                })
                                .setIcon(R.drawable.ic_check_black_24dp)
                                .setOnDismissListener(new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialog) {
                                        if(!check) {
                                            startActivity(new Intent(getApplicationContext(), NavigationActivity.class));
                                            finish();
                                        }
                                    }
                                })
                                .show();
                    } else
                        Toast.makeText(getApplicationContext(), "Cannot Remove Users!", Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    Log.e("RemoveResponse", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("RemoveResponse", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                LoginCookies loginCookies = SharedPrefManager.getInstance(getApplicationContext()).getLogin();
                params.put("X-CSRF-Token", loginCookies.getCsrfAccessCookie());
                params.put("Cookie", LoginCookies.JWT_ACCESS_COOKIE_NAME + "=" + loginCookies.getAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_COOKIE_NAME + "=" + loginCookies.getRefreshCookie() +
                        "; " + LoginCookies.JWT_ACCESS_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfRefreshCookie());
                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("community", String.valueOf(NavigationActivity.currentCommunityID) );
                params.put("users", grantUsers);
                Log.e("xD",grantUsers);
                grantUsers="";
                return params;
            }
        };

        requestQueue.add(removeUser);
    }



    private void verifyTokenStart() {
        if ((!ConnectionManager.isNetworkAvailable(this)) || (!ConnectionManager.hasInternetAccess(this)))
            return;
        StringRequest verify = new StringRequest(Request.Method.POST, ServerURL.VERIFY_TOKEN_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject responseObj = new JSONObject(response);
                            if (responseObj.getInt("success") != 1) refreshTokenStart();
                            else getUserList();
                        } catch (JSONException e) {
                            refreshTokenStart();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Do stuff
                        //Probably means 401 - so try refresh
                        refreshTokenStart();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                LoginCookies loginCookies = SharedPrefManager.getInstance(getApplicationContext()).getLogin();
                params.put("X-CSRF-Token", loginCookies.getCsrfAccessCookie());
                params.put("Cookie", LoginCookies.JWT_ACCESS_COOKIE_NAME + "=" + loginCookies.getAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_COOKIE_NAME + "=" + loginCookies.getRefreshCookie() +
                        "; " + LoginCookies.JWT_ACCESS_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfRefreshCookie());
                return params;
            }
        };
        requestQueue.add(verify);
    }

    private void refreshTokenStart() {
        final CookieManager manager = new CookieManager(null, CookiePolicy.ACCEPT_ALL);
        CookieHandler.setDefault(manager);
        StringRequest refresh = new StringRequest(Request.Method.POST, ServerURL.REFRESH_TOKEN_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject responseObj = new JSONObject(response);
                            if (!Objects.equals(responseObj.getString("refresh"), "true")) {
                                Toast.makeText(getApplicationContext(), "Please logout and login again", Toast.LENGTH_SHORT).show();
                            } else {
                                List<HttpCookie> cookieList = manager.getCookieStore().getCookies();
                                boolean accessCookieFound = false;
                                boolean accessCsrfFound = false;
                                Map<String, String> cookies = new HashMap<>();
                                for (HttpCookie cookie : cookieList) {
                                    if (cookie.getName().equals(LoginCookies.JWT_ACCESS_COOKIE_NAME)) {
                                        cookies.put(LoginCookies.JWT_ACCESS_COOKIE_NAME, cookie.getValue());
                                        accessCookieFound = true;
                                    }
                                    if (cookie.getName().equals(LoginCookies.JWT_ACCESS_CSRF_COOKIE_NAME)) {
                                        cookies.put(LoginCookies.JWT_ACCESS_CSRF_COOKIE_NAME, cookie.getValue());
                                        accessCsrfFound = true;
                                    }
                                    if (accessCookieFound && accessCsrfFound) {
                                        SharedPrefManager.getInstance(getApplicationContext()).setAccessCookie(cookies);
                                        break;
                                    }
                                }

                                if (accessCookieFound && accessCsrfFound)
                                    getUserList();
                                else
                                    Toast.makeText(getApplicationContext(), "Please logout and login again", Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException | NullPointerException e) {
                            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Do stuff
                        //this means that your refresh token has expired!
                        Toast.makeText(getApplicationContext(), "Please logout and login again", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                LoginCookies loginCookies = SharedPrefManager.getInstance(getApplicationContext()).getLogin();
                params.put("X-CSRF-Token", loginCookies.getCsrfRefreshCookie());
                params.put("Cookie", LoginCookies.JWT_ACCESS_COOKIE_NAME + "=" + loginCookies.getAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_COOKIE_NAME + "=" + loginCookies.getRefreshCookie() +
                        "; " + LoginCookies.JWT_ACCESS_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfRefreshCookie());
                return params;
            }
        };
        requestQueue.add(refresh);
    }
    private void verifyToken() {
        if ((!ConnectionManager.isNetworkAvailable(this)) || (!ConnectionManager.hasInternetAccess(this)))
            return;
        StringRequest verify = new StringRequest(Request.Method.POST, ServerURL.VERIFY_TOKEN_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject responseObj = new JSONObject(response);
                            if (responseObj.getInt("success") != 1) refreshTokenStart();
                            else getData();
                        } catch (JSONException e) {
                            refreshTokenStart();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Do stuff
                        //Probably means 401 - so try refresh
                        refreshToken();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                LoginCookies loginCookies = SharedPrefManager.getInstance(getApplicationContext()).getLogin();
                params.put("X-CSRF-Token", loginCookies.getCsrfAccessCookie());
                params.put("Cookie", LoginCookies.JWT_ACCESS_COOKIE_NAME + "=" + loginCookies.getAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_COOKIE_NAME + "=" + loginCookies.getRefreshCookie() +
                        "; " + LoginCookies.JWT_ACCESS_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfRefreshCookie());
                return params;
            }
        };
        requestQueue.add(verify);
    }

    private void refreshToken() {
        final CookieManager manager = new CookieManager(null, CookiePolicy.ACCEPT_ALL);
        CookieHandler.setDefault(manager);
        StringRequest refresh = new StringRequest(Request.Method.POST, ServerURL.REFRESH_TOKEN_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject responseObj = new JSONObject(response);
                            if (!Objects.equals(responseObj.getString("refresh"), "true")) {
                                Toast.makeText(getApplicationContext(), "Please logout and login again", Toast.LENGTH_SHORT).show();
                            } else {
                                List<HttpCookie> cookieList = manager.getCookieStore().getCookies();
                                boolean accessCookieFound = false;
                                boolean accessCsrfFound = false;
                                Map<String, String> cookies = new HashMap<>();
                                for (HttpCookie cookie : cookieList) {
                                    if (cookie.getName().equals(LoginCookies.JWT_ACCESS_COOKIE_NAME)) {
                                        cookies.put(LoginCookies.JWT_ACCESS_COOKIE_NAME, cookie.getValue());
                                        accessCookieFound = true;
                                    }
                                    if (cookie.getName().equals(LoginCookies.JWT_ACCESS_CSRF_COOKIE_NAME)) {
                                        cookies.put(LoginCookies.JWT_ACCESS_CSRF_COOKIE_NAME, cookie.getValue());
                                        accessCsrfFound = true;
                                    }
                                    if (accessCookieFound && accessCsrfFound) {
                                        SharedPrefManager.getInstance(getApplicationContext()).setAccessCookie(cookies);
                                        break;
                                    }
                                }

                                if (accessCookieFound && accessCsrfFound)
                                    getData();
                                else
                                    Toast.makeText(getApplicationContext(), "Please logout and login again", Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException | NullPointerException e) {
                            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Do stuff
                        //this means that your refresh token has expired!
                        Toast.makeText(getApplicationContext(), "Please logout and login again", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                LoginCookies loginCookies = SharedPrefManager.getInstance(getApplicationContext()).getLogin();
                params.put("X-CSRF-Token", loginCookies.getCsrfRefreshCookie());
                params.put("Cookie", LoginCookies.JWT_ACCESS_COOKIE_NAME + "=" + loginCookies.getAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_COOKIE_NAME + "=" + loginCookies.getRefreshCookie() +
                        "; " + LoginCookies.JWT_ACCESS_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfRefreshCookie());
                return params;
            }
        };
        requestQueue.add(refresh);
    }
}
