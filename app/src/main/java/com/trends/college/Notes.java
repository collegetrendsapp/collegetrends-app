package com.trends.college;

import android.net.Uri;

/**
 * basic skeleton class for storing material details
 */
class Notes {
    private int id;
    private String title;
    private String content;
    private String fname;
    private String lname;
    private String thread;
    private String timestamp;
    private Uri link;
    private String[] tags;

    /**
     * Notes constructor
     *
     * @param id        Material ID
     * @param title     Material title
     * @param content   Material body content
     * @param fname     Material uploaded by faculty first name
     * @param lname     Material uploaded by faculty last name
     * @param thread    Material community thread ID
     * @param timestamp Material upload time
     * @param link      Material link
     * @param tags      Material tags
     */
    Notes(int id, String title, String content, String fname, String lname, String thread, String timestamp, Uri link, String[] tags) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.fname = fname;
        this.lname = lname;
        this.thread = thread;
        this.timestamp = timestamp;
        this.link = link;
        for (int i = 0; i < tags.length; i++) tags[i] = tags[i].trim();
        this.tags = tags;
    }

    /**
     * Getter method for tags array
     *
     * @return Array of tags
     */
    String[] getTags() {
        return tags;
    }

    /**
     * Getter method for ID
     *
     * @return Material ID
     */
    int getId() {
        return id;
    }

    /**
     * Getter method for title
     * @return title
     */
    String getTitle() {
        return title;
    }

    /**
     * Getter method for content
     * @return content
     */
    String getContent() {
        return content;
    }

    /**
     * Getter method for faculty first name
     * @return first name
     */
    String getFname() {
        return fname;
    }

    /**
     * Getter method for faculty last name
     * @return faculty last name
     */
    String getLname() {
        return lname;
    }

    /**
     * Getter method for thread id
     * @return thread id
     */
    String getThread() {
        return thread;
    }

    /**
     * Getter method for upload time
     * @return upload time
     */
    String getTimestamp() {
        return timestamp.substring(0,timestamp.lastIndexOf(' ')).trim();
    }

    /**
     * Getter method for material link
     * @return material link
     */
    Uri getLink() {
        return link;
    }
}