package com.trends.college;

/**
 * Basic skeleton class for storing notification builder details for material
 */
class NotificationMaterial {
    private int id;
    private String topic;
    private String fname;
    private String lname;
    private String datetime;
    private String material_id;
    private String material_title;

    /**
     * Constructor for notification
     *
     * @param id             Notification ID
     * @param topic          Notification topic
     * @param fname          Notification faculty first name
     * @param lname          Notification faculty last name
     * @param datetime       Notification creation time
     * @param material_id    Notification material ID
     * @param material_title Notification material title
     */
    NotificationMaterial(int id, String topic, String fname, String lname, String datetime, String material_id, String material_title) {
        this.id = id;
        this.topic = topic;
        this.fname = fname;
        this.lname = lname;
        this.datetime = datetime;
        this.material_id = material_id;
        this.material_title = material_title;
    }

    /**
     * Material ID
     *
     * @return id
     */
    int getId() {
        return id;
    }

    /**
     * Material topic
     *
     * @return topic
     */
    String getTopic() {
        return topic;
    }

    /**
     * Faculty first name
     * @return first name
     */
    String getFname() {
        return fname;
    }

    /**
     * Faculty last name
     * @return last name
     */
    String getLname() {
        return lname;
    }

    /**
     * Creation time
     * @return time of creation
     */
    String getDatetime() {
        return datetime;
    }

    /**
     * Material ID
     * @return ID
     */
    String getMaterial_id() {
        return material_id;
    }

    /**
     * Material title
     * @return title
     */
    String getMaterial_title() {
        return material_title;
    }
}