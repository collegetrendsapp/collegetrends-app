package com.trends.college;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpCookie;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class RegisterAdminFragment extends Fragment {
    private String name;
    private String email;
    private String ph;
    private String pass;
    private String repass;
    private EditText nameView;
    private EditText emailView;
    private EditText phoneView;
    private EditText passView;
    private EditText repassView;
    private RequestQueue requestQueue;
    private Boolean check;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_register_admin, container, false);
        requestQueue = Volley.newRequestQueue(getContext());
        nameView = view.findViewById(R.id.input_name);
        emailView = view.findViewById(R.id.input_email);
        phoneView = view.findViewById(R.id.input_phone);
        passView = view.findViewById(R.id.input_password);
        repassView = view.findViewById(R.id.input_reEnterPassword);
        Button register = view.findViewById(R.id.btn_signup);
        check=false;
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name = nameView.getText().toString();
                email = emailView.getText().toString();
                ph = phoneView.getText().toString();
                pass = passView.getText().toString();
                repass = repassView.getText().toString();
                if (check()) registerUser();
            }
        });
        return view;
    }
    private boolean check() {
        if (name.length() == 0) {
            Snackbar.make(getActivity().getCurrentFocus(), "Enter name", Snackbar.LENGTH_LONG).setAction("Enter", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    nameView.requestFocus();
                }
            }).setActionTextColor(Color.WHITE).show();
            return false;
        } if (ph.length() != 10) {
            Snackbar.make(getActivity().getCurrentFocus(), "Enter 10 digit phone no.", Snackbar.LENGTH_LONG).setAction("Enter", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    phoneView.requestFocus();
                }
            }).setActionTextColor(Color.WHITE).show();
            return false;
        }
        if (!TextUtils.isEmpty(email) && !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            Snackbar.make(getActivity().getCurrentFocus(), "Invalid email", Snackbar.LENGTH_LONG).setAction("Enter", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    emailView.requestFocus();
                }
            }).setActionTextColor(Color.WHITE).show();
            return false;
        }
        if (pass.length() < 6) {
            Snackbar.make(getActivity().getCurrentFocus(), "Password requires atleast 6 characters", Snackbar.LENGTH_LONG).setAction("Enter", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    passView.requestFocus();
                }
            }).setActionTextColor(Color.WHITE).show();
            return false;
        }
        if (repass.length() == 0 || (!repass.equals(pass))) {
            Snackbar.make(getActivity().getCurrentFocus(), "Passwords do not match", Snackbar.LENGTH_LONG).setAction("Correct", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    repassView.requestFocus();
                }
            }).setActionTextColor(Color.WHITE).show();
            return false;
        }
        return true;
    }
    private void registerUser() {
        /*if ((!ConnectionManager.isNetworkAvailable(getActivity())) || (!ConnectionManager.hasInternetAccess(this)))
            return;*/
        String url = ServerURL.REGISTER_URL_ADMIN;
        StringRequest stringRequestRegister = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("RegResponse", response);
                        try {
                            JSONObject result = new JSONObject(response);
                            int code = result.getInt("response");
                            if (code == 0) {
                                Toast.makeText(getActivity(), "Registered successfully!", Toast.LENGTH_SHORT).show();
                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.DialogTheme);
                                builder.setTitle("Account Create")
                                        .setMessage("Admin Account is created")
                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                check = true;
                                                startActivity(new Intent(getActivity().getApplicationContext(), NavigationActivity.class));
                                                getActivity().finish();
                                            }
                                        })
                                        .setIcon(R.drawable.ic_check_black_24dp)
                                        .setOnDismissListener(new DialogInterface.OnDismissListener() {
                                            @Override
                                            public void onDismiss(DialogInterface dialog) {
                                                if (!check) {
                                                    startActivity(new Intent(getActivity().getApplicationContext(), NavigationActivity.class));
                                                    getActivity().finish();
                                                }
                                            }
                                        })
                                        .show();
                            } else
                                Toast.makeText(getActivity(), "Error registering", Toast.LENGTH_SHORT).show();
                        } catch (JSONException | NullPointerException e) {
                            Toast.makeText(getActivity(), "Error registering", Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getActivity(), "Error registering", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("fname", name);
                params.put("lname", "");
                params.put("email", "");
                params.put("mobile", ph);
                Log.e("PH", ph);
                params.put("pwd", pass);
                return params;
            }
        };
        requestQueue.add(stringRequestRegister);
    }

}
