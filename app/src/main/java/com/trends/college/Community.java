package com.trends.college;

/**
 * basic skeleton class for storing Community details
 */
class Community {
    private int id;
    private String name;

    /**
     * Community constructor
     *
     * @param id   Community ID
     * @param name Community name
     */
    Community(int id, String name) {
        this.id = id;
        this.name = name;
    }

    /**
     * Community ID
     *
     * @return ID
     */
    int getId() {
        return id;
    }

    /**
     * Community name
     *
     * @return Name
     */
    String getName() {
        return name;
    }
}