package com.trends.college;

/**
 * Stores list of URLs
 */
final class ServerURL {//stores list of URLs
    private static final String BASE_URL = "http://192.168.43.98:7000";
    static final String LOGIN_URL_STUDENT = BASE_URL + "/token/login/student";
    static final String LOGIN_URL_FACULTY = BASE_URL + "/token/login/faculty";
    static final String LOGIN_URL_ADMIN = BASE_URL + "/token/login/admin";
    static final String REGISTER_URL_FACULTY = BASE_URL + "/api/register/faculty";
    static final String REGISTER_URL_STUDENTS = BASE_URL + "/api/register/students";
    static final String REGISTER_URL_ADMIN = BASE_URL + "/api/register/admin";
    static final String LOGOUT_URL = BASE_URL + "/token/remove";
    static final String VERIFY_TOKEN_URL = BASE_URL + "/token/verify";
    static final String REFRESH_TOKEN_URL = BASE_URL + "/token/refresh";
    static final String GET_COMMUNITIES_URL = BASE_URL + "/api/communities/list";
    static final String GET_NOTES_URL = BASE_URL + "/api/materials/get";
    static final String GET_SCHEDULE_URL = BASE_URL + "/api/schedules/get";
    static final String UPLOAD_SCHEDULE_URL = BASE_URL + "/api/schedules/upload";
    static final String UPLOAD_NOTES_URL = BASE_URL + "/api/materials/upload";
    static final String GET_COMMENTS_URL = BASE_URL + "/api/discuss/view";
    static final String UPLOAD_COMMENT_URL = BASE_URL + "/api/discuss/comment";
    static final String GET_NOTIFICATIONS = BASE_URL + "/api/notifications/get";
    static final String LIST_USER = BASE_URL + "/api/communities/list_user";
    static final String ADD_COMMUNITY = BASE_URL + "/api/communities/create";
    static final String ADD_USER_COMMUNITY = BASE_URL + "/api/communities/add_users";
    static final String REMOVE_USER_COMMUNITY = BASE_URL + "/api/communities/remove_users";
    static final String LIST_USER_ADD = BASE_URL + "/api/communities/list_user_exc";
    static final String UPLOAD_CSV = BASE_URL + "/api/register/import";

    /**
     * Get URL of server
     *
     * @return server URL
     */
    public static String getBaseUrl() {
        return BASE_URL;
    }
}