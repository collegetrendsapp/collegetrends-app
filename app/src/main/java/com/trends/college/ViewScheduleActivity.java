package com.trends.college;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import net.idik.lib.slimadapter.SlimAdapter;
import net.idik.lib.slimadapter.SlimInjector;
import net.idik.lib.slimadapter.viewinjector.IViewInjector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpCookie;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class ViewScheduleActivity extends AppCompatActivity {
    static volatile RequestQueue requestQueue;
    private String timeStamp;
    private String linkText;
    private String threadID;
    private String title;
    private String content;
    private SlimAdapter slimAdapterComments;
    private RecyclerView recyclerViewComments;
    private ArrayList<Object> currentComments=new ArrayList<>();
    private String commentText;
    TextView commentCount;
    TextView contentView;
    NestedScrollView nestedScrollView;
    EditText commentTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_schedule);
        Intent intent = getIntent();
        linkText = intent.getStringExtra("link");
        timeStamp = intent.getStringExtra("timeStamp");
        threadID = intent.getStringExtra("thread");
        title = intent.getStringExtra("title");
        content = intent.getStringExtra("content");
        recyclerViewComments = findViewById(R.id.recycler_comments);
        LinearLayoutManager linearLayoutManagerComments = new LinearLayoutManager(this);
        linearLayoutManagerComments.canScrollVertically();
        linearLayoutManagerComments.setReverseLayout(true);
        nestedScrollView = findViewById(R.id.scrollView);
        recyclerViewComments.setLayoutManager(linearLayoutManagerComments);
        recyclerViewComments.setNestedScrollingEnabled(false);
        recyclerViewComments.setHasFixedSize(true);
        requestQueue = Volley.newRequestQueue(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        commentCount = findViewById(R.id.commentCount);
        commentTextView = findViewById(R.id.commentText);
        Button send = findViewById(R.id.send);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                commentText = commentTextView.getText().toString();
                if (!commentText.isEmpty()) {
                    verifyTokenComment();
                }

            }
        });
        TextView titleView = findViewById(R.id.title);
        contentView = findViewById(R.id.description);
        TextView timeStampView = findViewById(R.id.dateTime);
        titleView.setText(title);
        contentView.setText(content);
        timeStampView.setText(timeStamp);
        TextView linkView=findViewById(R.id.link);
        linkView.setText(linkText);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewScheduleActivity.super.onBackPressed();
            }
        });
        initSlimAdapter();


    }

    private void initSlimAdapter() {
        slimAdapterComments = SlimAdapter.create()
                .register(R.layout.layout_comment, new SlimInjector<Comment>() {
                    @Override
                    public void onInject(@NonNull final Comment data, @NonNull IViewInjector injector) {
                        injector.text(R.id.name, data.getFname() + " " + data.getLname())
                                .text(R.id.comment, data.getContent())
                                .text(R.id.timeStamp, data.getTimestamp())
                                .longClicked(R.id.name, new View.OnLongClickListener() {
                                    @Override
                                    public boolean onLongClick(View view) {
                                        //do whatever
                                        return false;
                                    }
                                })
                                .clicked(R.id.commentLayout, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                    }
                                });
                    }
                })
                .enableDiff()
                .attachTo(recyclerViewComments);

        verifyToken();
    }

    private void getData() {
        /*if ((!ConnectionManager.isNetworkAvailable(parentActivity)) || (!ConnectionManager.hasInternetAccess(parentActivity)))
            return;*/
        //hideKeyboard(parentActivity);
        StringRequest getComments = new StringRequest(Request.Method.POST, ServerURL.GET_COMMENTS_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("CommentsResponse", response);
                response = response.replaceAll("[\\\\]", "");
                response = response.replace("\"{", "{");
                response = response.replace("}\"", "}");
                try {
                    JSONArray userObj = new JSONArray(response);
                    JSONObject checkSuccess = userObj.getJSONObject(0);
                    int count = checkSuccess.getInt("count");
                    if (count == 1 || count == 0)
                        commentCount.setText(count + " Comment");
                    else
                        commentCount.setText(count + " Comments");

                    if (response != null && checkSuccess.getInt("success") == 1) {
                        userObj = userObj.getJSONArray(1);
                        for (int i = 0; i < userObj.length(); i++) {
                            JSONObject obj = userObj.getJSONObject(i);
                            String fname = obj.getString("user_fname");
                            String lname = obj.getString("user_lname");
                            String content = obj.getString("content");
                            String level = obj.getString("user_level");
                            String timestamp = obj.getString("timestamp");
                            int id = obj.getInt("user_id");
                            currentComments.add(new Comment(id, level, content, fname, lname, timestamp));
                        }
                        slimAdapterComments.updateData(currentComments);
                        slimAdapterComments.updateData(currentComments);
                        Log.e("Comment count", currentComments.size() + " ");
                        if (!commentTextView.getText().toString().isEmpty()) {
                            commentTextView.setText("");
                            commentTextView.setText("");
                            Log.e("Comment count", currentComments.size() + " " + slimAdapterComments.getItemCount());
                            hideKeyboard(ViewScheduleActivity.this);
                        }
                    }
                } catch (JSONException e) {
                    Log.e("CommentsResponse", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("CommentsErrorResponse", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                LoginCookies loginCookies = SharedPrefManager.getInstance(ViewScheduleActivity.this).getLogin();
                params.put("X-CSRF-Token", loginCookies.getCsrfAccessCookie());
                params.put("Cookie", LoginCookies.JWT_ACCESS_COOKIE_NAME + "=" + loginCookies.getAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_COOKIE_NAME + "=" + loginCookies.getRefreshCookie() +
                        "; " + LoginCookies.JWT_ACCESS_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfRefreshCookie());
                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("thread", threadID);
                return params;
            }
        };
        requestQueue.add(getComments);
    }

    private void verifyToken() {
        /*if ((!ConnectionManager.isNetworkAvailable(parentActivity)) || (!ConnectionManager.hasInternetAccess(parentActivity)))
            return;*/
        currentComments = new ArrayList<>();
        StringRequest verify = new StringRequest(Request.Method.POST, ServerURL.VERIFY_TOKEN_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject responseObj = new JSONObject(response);
                            if (responseObj.getInt("success") != 1) refreshToken();
                            else {
                                getData();
                            }
                        } catch (JSONException e) {
                            refreshToken();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Do stuff
                        //Probably means 401 - so try refresh
                        refreshToken();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                LoginCookies loginCookies = SharedPrefManager.getInstance(ViewScheduleActivity.this).getLogin();
                params.put("X-CSRF-Token", loginCookies.getCsrfAccessCookie());
                params.put("Cookie", LoginCookies.JWT_ACCESS_COOKIE_NAME + "=" + loginCookies.getAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_COOKIE_NAME + "=" + loginCookies.getRefreshCookie() +
                        "; " + LoginCookies.JWT_ACCESS_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfRefreshCookie());
                return params;
            }
        };
        requestQueue.add(verify);
    }

    private void refreshToken() {
        final CookieManager manager = new CookieManager(null, CookiePolicy.ACCEPT_ALL);
        CookieHandler.setDefault(manager);
        StringRequest refresh = new StringRequest(Request.Method.POST, ServerURL.REFRESH_TOKEN_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject responseObj = new JSONObject(response);
                            if (!Objects.equals(responseObj.getString("refresh"), "true")) {
                                Toast.makeText(ViewScheduleActivity.this, "Please logout and login again", Toast.LENGTH_SHORT).show();
                            } else {
                                List<HttpCookie> cookieList = manager.getCookieStore().getCookies();
                                boolean accessCookieFound = false;
                                boolean accessCsrfFound = false;
                                Map<String, String> cookies = new HashMap<>();
                                for (HttpCookie cookie : cookieList) {
                                    if (cookie.getName().equals(LoginCookies.JWT_ACCESS_COOKIE_NAME)) {
                                        cookies.put(LoginCookies.JWT_ACCESS_COOKIE_NAME, cookie.getValue());
                                        accessCookieFound = true;
                                    }
                                    if (cookie.getName().equals(LoginCookies.JWT_ACCESS_CSRF_COOKIE_NAME)) {
                                        cookies.put(LoginCookies.JWT_ACCESS_CSRF_COOKIE_NAME, cookie.getValue());
                                        accessCsrfFound = true;
                                    }
                                    if (accessCookieFound && accessCsrfFound) {
                                        SharedPrefManager.getInstance(ViewScheduleActivity.this).setAccessCookie(cookies);
                                        break;
                                    }
                                }

                                if (accessCookieFound && accessCsrfFound)
                                    getData();
                                else
                                    Toast.makeText(ViewScheduleActivity.this, "Please logout and login again", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException | NullPointerException e) {
                            Toast.makeText(ViewScheduleActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Do stuff
                        //this means that your refresh token has expired!
                        Toast.makeText(ViewScheduleActivity.this, "Please logout and login again", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                LoginCookies loginCookies = SharedPrefManager.getInstance(ViewScheduleActivity.this).getLogin();
                params.put("X-CSRF-Token", loginCookies.getCsrfRefreshCookie());
                params.put("Cookie", LoginCookies.JWT_ACCESS_COOKIE_NAME + "=" + loginCookies.getAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_COOKIE_NAME + "=" + loginCookies.getRefreshCookie() +
                        "; " + LoginCookies.JWT_ACCESS_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfRefreshCookie());
                return params;
            }
        };
        requestQueue.add(refresh);
    }

    private void uploadComment() {
        /*if ((!ConnectionManager.isNetworkAvailable(this)) || (!ConnectionManager.hasInternetAccess(this)))
            return;*/
        StringRequest uploadComment = new StringRequest(Request.Method.POST, ServerURL.UPLOAD_COMMENT_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("CommentUploadResponse", response);//response -5 sometimes
                try {
                    JSONObject checkSuccess = new JSONObject(response);
                    int success = checkSuccess.getInt("success");
                    if (success == 1) {
                        verifyToken();
                        Log.e("UploadSuccess", success + "");
                    } else
                        Toast.makeText(getApplicationContext(), "Cannot Comment here", Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    Log.e("CommentUploadResponse", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("CommentUploadResponse", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                LoginCookies loginCookies = SharedPrefManager.getInstance(getApplicationContext()).getLogin();
                params.put("X-CSRF-Token", loginCookies.getCsrfAccessCookie());
                params.put("Cookie", LoginCookies.JWT_ACCESS_COOKIE_NAME + "=" + loginCookies.getAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_COOKIE_NAME + "=" + loginCookies.getRefreshCookie() +
                        "; " + LoginCookies.JWT_ACCESS_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfRefreshCookie());
                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                if (!commentText.isEmpty()) {
                    params.put("content", commentText);
                    params.put("thread", threadID);
                    commentText = "";
                }
                return params;
            }
        };

        requestQueue.add(uploadComment);
    }

    private void verifyTokenComment() {
        if ((!ConnectionManager.isNetworkAvailable(this)) || (!ConnectionManager.hasInternetAccess(this)))
            return;
        StringRequest verify = new StringRequest(Request.Method.POST, ServerURL.VERIFY_TOKEN_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject responseObj = new JSONObject(response);
                            if (responseObj.getInt("success") != 1) refreshToken();
                            else uploadComment();
                        } catch (JSONException e) {
                            refreshToken();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Do stuff
                        //Probably means 401 - so try refresh
                        refreshToken();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                LoginCookies loginCookies = SharedPrefManager.getInstance(getApplicationContext()).getLogin();
                params.put("X-CSRF-Token", loginCookies.getCsrfAccessCookie());
                params.put("Cookie", LoginCookies.JWT_ACCESS_COOKIE_NAME + "=" + loginCookies.getAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_COOKIE_NAME + "=" + loginCookies.getRefreshCookie() +
                        "; " + LoginCookies.JWT_ACCESS_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfRefreshCookie());
                return params;
            }
        };
        requestQueue.add(verify);

    }

    void hideKeyboard(Activity activity) {
        InputMethodManager inputMethodManager;
        inputMethodManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        View currentFocusedView = activity.getCurrentFocus();
        if (currentFocusedView != null && inputMethodManager != null)
            inputMethodManager.hideSoftInputFromWindow(currentFocusedView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }
}