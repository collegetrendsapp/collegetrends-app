package com.trends.college;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.firebase.jobdispatcher.JobParameters;
import com.firebase.jobdispatcher.JobService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

public class NotificationService extends JobService {

    private static final String CHANNEL_ID = "notification";
    private ArrayList<Object> notifs;
    //private ArrayList<String> notifsComm;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public boolean onStartJob(JobParameters job) {
        Log.e("Job", "Start");
        getNotif();
        return false;
    }

    @Override
    public boolean onStopJob(JobParameters job) {
        Log.e("Job", "Stopped");
        return true;
    }

    private void getNotif() {
        Log.e("VolleyMethod: ", "Called");
        final CountDownLatch countDownLatch = new CountDownLatch(1);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ServerURL.GET_NOTIFICATIONS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                notifs = new ArrayList<>();
                //notifsComm = new ArrayList<>();
                Log.e("RespSUCEESS", response);
                response = response.replaceAll("[\\\\]", "");
                response = response.replace("\"{", "{");
                response = response.replace("}\"", "}");
                try {
                    JSONArray userObj = new JSONArray(response);
                    JSONObject checkSuccess = userObj.getJSONObject(0);
                    if (response != null && checkSuccess.getInt("success") == 1 && checkSuccess.getInt("count") > 0) {
                        userObj = userObj.getJSONArray(1);
                        for (int i = 0; i < userObj.length(); i++) {
                            JSONObject obj = userObj.getJSONObject(i);
                            int id = obj.getInt("id");
                            String topic = obj.getString("topic");
                            String fname = obj.getString("faculty_fname");
                            String lname = obj.getString("faculty_lname");
                            String datetime = obj.getString("datetime");
                            if (obj.has("material_id") && obj.has("material_title"))
                                notifs.add(new NotificationMaterial(id, topic, fname, lname, datetime,
                                        obj.getString("material_id"), obj.getString("material_title")));
                            else if (obj.has("schedule_id") && obj.has("schedule_title"))
                                notifs.add(new NotificationSchedule(id, topic, fname, lname, datetime,
                                        obj.getString("schedule_id"), obj.getString("schedule_title")));
                            else
                                notifs.add(new NotificationOther(id, topic, fname, lname, datetime));
                        }
                        /*for (int i = userObj.length() / 2; i < userObj.length(); i++) {
                            JSONObject obj = userObj.getJSONObject(i);
                            int id = obj.getInt("id");
                            int community = obj.getInt("community");
                            String topic = obj.getString("topic");
                            String fname = obj.getString("faculty_fname");
                            String lname = obj.getString("faculty_lname");
                            String datetime = obj.getString("datetime");
                            notifsComm.add(topic);
                        }*/
                    }
                } catch (JSONException e) {
                    Log.e("JSONNotifExcep", e.toString());
                }
                Intent intent = new Intent(getApplicationContext(), NavigationActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, 0);

                int noMat = 0, noSched = 0, noOthers = 0;
                for (int i = 0; i < notifs.size(); i++) {
                    noMat = notifs.get(i) instanceof NotificationMaterial ? noMat + 1 : noMat;
                    noSched = notifs.get(i) instanceof NotificationSchedule ? noSched + 1 : noSched;
                    noOthers = notifs.get(i) instanceof NotificationOther ? noOthers + 1 : noOthers;
                }
                String text = (noMat > 0) ? noMat + " New Material Uploaded" : "";
                text = (noSched > 0) ? text + "\n" + noSched + " New Schedule Uploaded" : text;
                text = (noOthers > 0) ? text + "\n" + noOthers + " Other Notifications" : text;
                StringBuilder bigText = new StringBuilder();
                for (int i = 0; i < notifs.size(); i++) {
                    if (notifs.get(i) instanceof NotificationMaterial) {
                        NotificationMaterial obj = (NotificationMaterial) notifs.get(i);
                        bigText.append(obj.getMaterial_title()).append(" uploaded by ")
                                .append(obj.getFname()).append(" ").append(obj.getLname()).append("\n");
                        //.append(" in community ").append(notifsComm.get(i)).append("\n");
                    } else if (notifs.get(i) instanceof NotificationSchedule) {
                        NotificationSchedule obj = (NotificationSchedule) notifs.get(i);
                        bigText.append(obj.getSchedule_title()).append(" posted by ")
                                .append(obj.getFname()).append(" ").append(obj.getLname()).append("\n");
                        //.append(" in community ").append(notifsComm.get(i)).append("\n");
                    } else {
                        NotificationOther obj = (NotificationOther) notifs.get(i);
                        bigText.append(obj.getTopic()).append(" posted by ")
                                .append(obj.getFname()).append(" ").append(obj.getLname()).append("\n");
                        //.append(" in community ").append(notifsComm.get(i)).append("\n");
                    }
                }

                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext(), CHANNEL_ID)
                        .setSmallIcon(R.drawable.announcement)
                        //.setLargeIcon(R.drawable.logo)
                        .setContentTitle("New notification")
                        .setContentText(text)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(bigText))
                        .setContentIntent(pendingIntent)
                        .setAutoCancel(true)
                        .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                        .setPriority(Notification.PRIORITY_MAX);
                NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                // notificationId is a unique int for each notification that you must define
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    NotificationChannel channel = new NotificationChannel(CHANNEL_ID, "College Trends",
                            NotificationManager.IMPORTANCE_DEFAULT);
                    notificationManager.createNotificationChannel(channel);
                }
                if (text.length() > 1)
                    notificationManager.notify(69, mBuilder.build());
                countDownLatch.countDown();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("RespERROR", error.toString());
                countDownLatch.countDown();
            }
        }) {
            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                Log.e("NetResponse", response.toString());
                countDownLatch.countDown();
                return super.parseNetworkResponse(response);
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                Log.e("NetError", volleyError.toString());
                countDownLatch.countDown();
                return super.parseNetworkError(volleyError);
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                LoginCookies loginCookies = SharedPrefManager.getInstance(getApplicationContext()).getLogin();
                params.put("X-CSRF-Token", loginCookies.getCsrfAccessCookie());
                params.put("Cookie", LoginCookies.JWT_ACCESS_COOKIE_NAME + "=" + loginCookies.getAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_COOKIE_NAME + "=" + loginCookies.getRefreshCookie() +
                        "; " + LoginCookies.JWT_ACCESS_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfRefreshCookie());
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
        try {
            countDownLatch.await();
        } catch (InterruptedException ignored) {
        }
    }
}