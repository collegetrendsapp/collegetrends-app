package com.trends.college;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.OpenFileActivityBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpCookie;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TimeZone;

/**
 * Faculty can upload schedules here
 */
public class ScheduleUploadActivity extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    static volatile RequestQueue requestQueue;
    private String title;
    private String desc;
    private String url;
    private Button setTime;
    private Button setDate;
    private String chosenDate;
    private String chosenTime;
    private Boolean check;
    private GoogleApiClient googleApiClient;
    private static final int REQUEST_CODE_SELECT = 102;
    private static final int REQUEST_CODE_RESOLUTION = 103;
    private static final String TAG = "Drive";

    /**
     * Default onCreate() overriden
     *
     * @param savedInstanceState Modify to store persistently
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);//initializing IDs
        check = false;
        setContentView(R.layout.activity_schedule_upload);
        final EditText titleEditText = findViewById(R.id.title);
        final EditText descEditText = findViewById(R.id.description);
        final EditText urlEditText = findViewById(R.id.url);
        Button upload = findViewById(R.id.upload);
        ImageView back = findViewById(R.id.backButton);
        requestQueue = Volley.newRequestQueue(this);
        back.setOnClickListener(v -> {//adding listeners
            startActivity(new Intent(getApplicationContext(), NavigationActivity.class));
            finish();
        });
        ImageButton link= findViewById(R.id.link);
        link.setOnClickListener(v -> {
            buildGoogleApiClient();
            if (googleApiClient != null) {
                googleApiClient.disconnect();
            }
            googleApiClient.connect();
        });
        setTime = findViewById(R.id.timeText);
        setDate = findViewById(R.id.validText);
        setDate.setOnClickListener(view -> {
            final Calendar c = Calendar.getInstance();
            int mYear = c.get(Calendar.YEAR);
            int mMonth = c.get(Calendar.MONTH);
            int mDay = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(ScheduleUploadActivity.this, R.style.DialogTheme, (view1, year, monthOfYear, dayOfMonth) -> {
                monthOfYear++;
                chosenDate = (dayOfMonth < 10 ? "0" : "") + dayOfMonth + "/" + (monthOfYear < 10 ? "0" : "") + monthOfYear + "/" + year;
                setDate.setText(String.format("Valid till %s", chosenDate));
            }, mYear, mMonth, mDay);
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
            datePickerDialog.show();
        });
        setTime.setOnClickListener(view -> {
            final Calendar c = Calendar.getInstance();
            int mHour = c.get(Calendar.HOUR_OF_DAY);
            int mMinute = c.get(Calendar.MINUTE);
            TimePickerDialog timePickerDialog = new TimePickerDialog(ScheduleUploadActivity.this, R.style.DialogTheme,
                    (view12, hourOfDay, minute) -> {
                        TimeZone tz = TimeZone.getDefault();
                        Date now = new Date();
                        double offsetFromUtc = tz.getOffset(now.getTime()) / 3600000.0;
                        boolean isNegative = (offsetFromUtc < 0);
                        offsetFromUtc = Math.abs(offsetFromUtc);
                        double min = (offsetFromUtc - (int) offsetFromUtc) * 60;
                        int hrs = (int) offsetFromUtc;
                        int mins = (int) min;

                        chosenTime = (hourOfDay < 10 ? "0" : "") + hourOfDay + ":" +
                                (minute < 10 ? "0" : "") + minute + ":00 " +
                                (isNegative ? "-" : "+") + (hrs < 10 ? "0" : "") + hrs + (mins < 10 ? "0" : "") + mins;
                        String chosenTime = "Expiry Time " + ((hourOfDay % 12 == 0) ? "12" : hourOfDay % 12) + ":" +
                                (minute < 10 ? "0" : "") + minute + (hourOfDay * 100 + minute >= 1200 ? " PM" : " AM");
                        setTime.setText(chosenTime);
                    }, mHour, mMinute, false);

            timePickerDialog.show();
        });
        upload.setOnClickListener(v -> {
            title = titleEditText.getText().toString();
            desc = descEditText.getText().toString();
            url = urlEditText.getText().toString();
            if (checkData()) verifyToken();
        });
    }

    /**
     * Checks validity before upload
     *
     * @return true if all details supplied for schedule
     */
    private boolean checkData() {//error checks
        if (title == null || title.length() == 0) {
            Toast.makeText(getApplicationContext(), "Title cannot be empty", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (desc == null || desc.length() == 0) {
            Toast.makeText(getApplicationContext(), "Description cannot be empty", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (url == null || url.length() == 0) {
            Toast.makeText(getApplicationContext(), "URL must be specified", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!Patterns.WEB_URL.matcher(url).matches()) {
            Toast.makeText(getApplicationContext(), "Invalid URL", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (chosenDate == null || chosenDate.length() < 1) {
            Toast.makeText(getApplicationContext(), "Validity date must be selected", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    /**
     * Opens default activity for logged-in session
     */
    @Override
    public void onBackPressed() { //custom override for back press action
        startActivity(new Intent(getApplicationContext(), NavigationActivity.class));
        finish();
    }

    /**
     * HTTP request to upload schedule
     */
    private void getData() {
        if ((!ConnectionManager.isNetworkAvailable(this)) || (!ConnectionManager.hasInternetAccess(this)))
            return;
        StringRequest uploadSchedule = new StringRequest(Request.Method.POST, ServerURL.UPLOAD_SCHEDULE_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("ScheduleUploadResponse", response);
                try {
                    JSONObject checkSuccess = new JSONObject(response);
                    int success = checkSuccess.getInt("success");
                    if (success >= 1) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(ScheduleUploadActivity.this, R.style.DialogTheme);
                        builder.setTitle("Schedule Upload Success")
                                .setMessage("This Schedule is created")
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        check = true;
                                        startActivity(new Intent(getApplicationContext(), NavigationActivity.class));
                                        finish();
                                    }
                                })
                                .setIcon(R.drawable.ic_check_black_24dp)
                                .setOnDismissListener(new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialog) {
                                        if (!check) {
                                            startActivity(new Intent(getApplicationContext(), NavigationActivity.class));
                                            finish();
                                        }
                                    }
                                })
                                .show();
                        Toast.makeText(getApplicationContext(), "Upload success", Toast.LENGTH_SHORT).show();
                    } else
                        Toast.makeText(getApplicationContext(), "Cannot post in this community", Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    Log.e("ScheduleUploadResponse", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ScheduleUploadResponse", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                LoginCookies loginCookies = SharedPrefManager.getInstance(getApplicationContext()).getLogin();
                params.put("X-CSRF-Token", loginCookies.getCsrfAccessCookie());
                params.put("Cookie", LoginCookies.JWT_ACCESS_COOKIE_NAME + "=" + loginCookies.getAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_COOKIE_NAME + "=" + loginCookies.getRefreshCookie() +
                        "; " + LoginCookies.JWT_ACCESS_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfRefreshCookie());
                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("title", title);
                params.put("content", desc);
                params.put("link", url);
                if (chosenTime == null || chosenTime.length() < 4) chosenTime = "12:00:00 +0530";
                params.put("datetime", chosenDate + " " + chosenTime);
                params.put("community", String.valueOf(NavigationActivity.currentCommunityID));
                return params;
            }
        };

        ScheduleUploadActivity.requestQueue.add(uploadSchedule);
    }

    /**
     * Verifies current session cookies
     */
    private void verifyToken() {
        if ((!ConnectionManager.isNetworkAvailable(this)) || (!ConnectionManager.hasInternetAccess(this)))
            return;
        StringRequest verify = new StringRequest(Request.Method.POST, ServerURL.VERIFY_TOKEN_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject responseObj = new JSONObject(response);
                            if (responseObj.getInt("success") != 1) refreshToken();
                            else getData();
                        } catch (JSONException e) {
                            refreshToken();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Do stuff
                        //Probably means 401 - so try refresh
                        refreshToken();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                LoginCookies loginCookies = SharedPrefManager.getInstance(getApplicationContext()).getLogin();
                params.put("X-CSRF-Token", loginCookies.getCsrfAccessCookie());
                params.put("Cookie", LoginCookies.JWT_ACCESS_COOKIE_NAME + "=" + loginCookies.getAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_COOKIE_NAME + "=" + loginCookies.getRefreshCookie() +
                        "; " + LoginCookies.JWT_ACCESS_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfRefreshCookie());
                return params;
            }
        };
        ScheduleUploadActivity.requestQueue.add(verify);
    }

    /**
     * Refresh cookies if expired
     */
    private void refreshToken() {
        final CookieManager manager = new CookieManager(null, CookiePolicy.ACCEPT_ALL);
        CookieHandler.setDefault(manager);
        StringRequest refresh = new StringRequest(Request.Method.POST, ServerURL.REFRESH_TOKEN_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject responseObj = new JSONObject(response);
                            if (!Objects.equals(responseObj.getString("refresh"), "true")) {
                                Toast.makeText(getApplicationContext(), "Please logout and login again", Toast.LENGTH_SHORT).show();
                            } else {
                                List<HttpCookie> cookieList = manager.getCookieStore().getCookies();
                                boolean accessCookieFound = false;
                                boolean accessCsrfFound = false;
                                Map<String, String> cookies = new HashMap<>();
                                for (HttpCookie cookie : cookieList) {
                                    if (cookie.getName().equals(LoginCookies.JWT_ACCESS_COOKIE_NAME)) {
                                        cookies.put(LoginCookies.JWT_ACCESS_COOKIE_NAME, cookie.getValue());
                                        accessCookieFound = true;
                                    }
                                    if (cookie.getName().equals(LoginCookies.JWT_ACCESS_CSRF_COOKIE_NAME)) {
                                        cookies.put(LoginCookies.JWT_ACCESS_CSRF_COOKIE_NAME, cookie.getValue());
                                        accessCsrfFound = true;
                                    }
                                    if (accessCookieFound && accessCsrfFound) {
                                        SharedPrefManager.getInstance(getApplicationContext()).setAccessCookie(cookies);
                                        break;
                                    }
                                }

                                if (accessCookieFound && accessCsrfFound)
                                    getData();
                                else
                                    Toast.makeText(getApplicationContext(), "Please logout and login again", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException | NullPointerException e) {
                            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Do stuff
                        //this means that your refresh token has expired!
                        Toast.makeText(getApplicationContext(), "Please logout and login again", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                LoginCookies loginCookies = SharedPrefManager.getInstance(getApplicationContext()).getLogin();
                params.put("X-CSRF-Token", loginCookies.getCsrfRefreshCookie());
                params.put("Cookie", LoginCookies.JWT_ACCESS_COOKIE_NAME + "=" + loginCookies.getAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_COOKIE_NAME + "=" + loginCookies.getRefreshCookie() +
                        "; " + LoginCookies.JWT_ACCESS_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfRefreshCookie());
                return params;
            }
        };
        ScheduleUploadActivity.requestQueue.add(refresh);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CODE_SELECT:
                if (resultCode == RESULT_OK) {
                    /*get the selected item's ID*/
                    DriveId driveId = data.getParcelableExtra(
                            OpenFileActivityBuilder.EXTRA_RESPONSE_DRIVE_ID);//this extra contains the drive id of the selected file
                    Log.i("Drive", "Selected folder's ID: " + driveId.encodeToString());
                    Log.i("Drive", "Selected folder's Resource ID: " + driveId.getResourceId());// this is the id of the actual file
                    EditText urlEditText = findViewById(R.id.url);
                    urlEditText.setText("https://drive.google.com/open?id="+driveId.getResourceId(), TextView.BufferType.EDITABLE);
                    googleApiClient.disconnect();
                    break;
                }
            case REQUEST_CODE_RESOLUTION:
                if (resultCode == RESULT_OK) {
                    googleApiClient.connect();
                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }

    /*connect client to Google Play Services*/
    @Override
    protected void onStart() {
        super.onStart();
        //googleApiClient.connect();
    }

    /*close connection to Google Play Services*/
    @Override
    protected void onStop() {
        super.onStop();
        if (googleApiClient != null) {
            googleApiClient.disconnect();
        }
    }
    private void buildGoogleApiClient() {
        Log.i("Drive", "Building the client");
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(Drive.API)
                    .addScope(Drive.SCOPE_FILE)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();
        }
    }

    /*Connection callback - on successful connection*/
    @Override
    public void onConnected(Bundle bundle) {
//        build an intent that we'll use to start the open file activity
        Log.i("drive", "Unable to send intent");
        IntentSender intentSender = Drive.DriveApi
                .newOpenFileActivityBuilder()
//                these mimetypes enable these folders/files types to be selected
                //.setMimeType(new String[] { DriveFolder.MIME_TYPE, "text/plain", "image/png"})
                .build(googleApiClient);
        try {
            startIntentSenderForResult(
                    intentSender, REQUEST_CODE_SELECT, null, 0, 0, 0);


        } catch (IntentSender.SendIntentException e) {
            Log.i("drive", "Unable to send intent");
        }
    }


    /*Connection callback - Called when the client is temporarily in a disconnected state*/
    @Override
    public void onConnectionSuspended(int i) {
        switch (i) {
            case 1:
                Log.i(TAG, "Connection suspended - Cause: " + "Service disconnected");
                break;
            case 2:
                Log.i(TAG, "Connection suspended - Cause: " + "Connection lost");
                break;
            default:
                Log.i(TAG, "Connection suspended - Cause: " + "Unknown");
                break;
        }
    }

    /*connection failed callback - Called when there was an error connecting the client to the service*/
    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.i(TAG, "Connection failed - result: " + result.toString());
        if (!result.hasResolution()) {
//            display error dialog
            GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), this, 0).show();
            return;
        }

        try {
            Log.i(TAG, "trying to resolve the Connection failed error...");
//            tries to resolve the connection failure by trying to restart this activity
            result.startResolutionForResult(this, REQUEST_CODE_RESOLUTION);
        } catch (IntentSender.SendIntentException e) {
            Log.i(TAG, "Exception while starting resolution activity", e);
        }
    }
    /*Connection callback - Called when the client is temporarily in a disconnected state*/
}