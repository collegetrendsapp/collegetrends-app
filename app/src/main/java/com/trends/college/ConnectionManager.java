package com.trends.college;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ExecutionException;

class ConnectionManager {
    /**
     *
     * @param activity
     * @return
     */
    static boolean isNetworkAvailable(FragmentActivity activity) {//checks if phone is connected to a network, NOT internet
        ConnectivityManager connectivityManager
                = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager != null ? connectivityManager.getActiveNetworkInfo() : null;
        if (activeNetworkInfo == null)
            Toast.makeText(activity.getApplicationContext(), "Check network connection", Toast.LENGTH_SHORT).show();//displayed if not connected
        return activeNetworkInfo != null;
    }

    static boolean hasInternetAccess(FragmentActivity activity) {//checks if phone is connected to the internet, should always be called after isNetworkAvailable()
        boolean b = false;
        ConnectionTask connectionTask = new ConnectionTask();
        connectionTask.execute();
        try {
            b = connectionTask.get();// result of AsyncTask pauses exec of main thread. Possible cause of ANR
        } catch (InterruptedException | ExecutionException e) {
            Log.e("Async", e.toString());
        }
        if (!b)
            Toast.makeText(activity.getApplicationContext(), "Check internet connection", Toast.LENGTH_SHORT).show();//displayed if not connected to internet
        return b;
    }

    /**
     * Checks if the network connection is available or not using AsyncTask so that the UI thread is not blocked.
     */
    private static class ConnectionTask extends AsyncTask<Void, Boolean, Boolean> {

        @Override
        protected Boolean doInBackground(Void... voids) {
            boolean result = false;
            try {
                HttpURLConnection urlc = (HttpURLConnection)
                        (new URL("http://clients3.google.com/generate_204") //204 checks cannot be made directly on the main thread
                                .openConnection());
                urlc.setRequestProperty("User-Agent", "Android");
                urlc.setRequestProperty("Connection", "close");
                urlc.setConnectTimeout(1500);
                urlc.connect();
                result = (urlc.getResponseCode() == 204 && urlc.getContentLength() == 0);
            } catch (IOException e) {
                Log.e("", "Error checking internet connection", e);
            }
            return result;
        }
    }
}