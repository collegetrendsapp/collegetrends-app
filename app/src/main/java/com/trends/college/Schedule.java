package com.trends.college;

import android.net.Uri;

class Schedule { //basic skeleton class for storing schedule details
    private int id;
    private String title;
    private String content;
    private String fname;
    private String lname;
    private String thread;
    private String timestamp;
    private Uri link;

    Schedule(int id, String title, String content, String fname, String lname, String thread, String timestamp, Uri link) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.fname = fname;
        this.lname = lname;
        this.thread = thread;
        this.timestamp = timestamp;
        this.link = link;
    }

    int getId() {
        return id;
    }

    String getTitle() {
        return title;
    }

    String getContent() {
        return content;
    }

    String getFname() {
        return fname;
    }

    String getLname() {
        return lname;
    }

    String getThread() {
        return thread;
    }

    String getTimestamp() {
        return timestamp.substring(0,timestamp.lastIndexOf(' ')).trim();
    }

    Uri getLink() {
        return link;
    }
}