package com.trends.college;

class NotificationSchedule { //basic skeleton class for storing notification builder details for schedule
    private int id;
    private String topic;
    private String fname;
    private String lname;
    private String datetime;
    private String schedule_id;
    private String schedule_title;

    NotificationSchedule(int id, String topic, String fname, String lname, String datetime, String schedule_id, String schedule_title) {
        this.id = id;
        this.topic = topic;
        this.fname = fname;
        this.lname = lname;
        this.datetime = datetime;
        this.schedule_id = schedule_id;
        this.schedule_title = schedule_title;
    }

    int getId() {
        return id;
    }

    String getTopic() {
        return topic;
    }

    String getFname() {
        return fname;
    }

    String getLname() {
        return lname;
    }

    String getDatetime() {
        return datetime;
    }

    String getSchedule_id() {
        return schedule_id;
    }

    String getSchedule_title() {
        return schedule_title;
    }
}