package com.trends.college;

class NotificationOther { //basic skeleton class for storing notification builder details for miscellaneous
    private int id;
    private String topic;
    private String fname;
    private String lname;
    private String datetime;

    NotificationOther(int id, String topic, String fname, String lname, String datetime) {
        this.id = id;
        this.topic = topic;
        this.fname = fname;
        this.lname = lname;
        this.datetime = datetime;
    }

    int getId() {
        return id;
    }

    String getTopic() {
        return topic;
    }

    String getFname() {
        return fname;
    }

    String getLname() {
        return lname;
    }

    String getDatetime() {
        return datetime;
    }
}