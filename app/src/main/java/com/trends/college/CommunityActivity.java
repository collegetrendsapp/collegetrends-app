package com.trends.college;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;


import org.json.JSONException;
import org.json.JSONObject;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpCookie;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import io.apptik.widget.multiselectspinner.MultiSelectSpinner;


public class CommunityActivity extends AppCompatActivity {
    private String users;
    private String usernames;
    static volatile RequestQueue requestQueue;
    private List<String> items;
    private List<String> itemsUserName;
    MultiSelectSpinner multiSelectSpinner;
    private RadioGroup radioGroup;
    private RadioButton radioButton;
    private Button create;
    private String name;
    private EditText nameComm;
    private String flag;
    private String grantUsers;
    private boolean[] checked;
    private Boolean check;

    /**
     *
     * @param savedInstanceState
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_community);
        requestQueue = Volley.newRequestQueue(this);
        multiSelectSpinner = findViewById(R.id.multiselectSpinner);
        radioGroup=findViewById(R.id.radioGroup);
        create=findViewById(R.id.create);
        nameComm=findViewById(R.id.name);
        grantUsers="";
        check=false;
        ImageView back = findViewById(R.id.backButton);
        requestQueue = Volley.newRequestQueue(this);
        back.setOnClickListener(v -> {
            startActivity(new Intent(getApplicationContext(), NavigationActivity.class));
            finish();
        });
        create.setOnClickListener(view -> {
            name=nameComm.getText().toString();
            if(name.equals("")){
                Toast.makeText(CommunityActivity.this, "Name of the community can not be empty.", Toast.LENGTH_SHORT).show();
                return;
            }
            int trueSize=0;
            int size=items.size();
            for(int i=0;i<size;i++){
                if(checked[i])
                    trueSize+=1;
            }
            for(int i=0,j=0;i<size;i++){
                if(checked[i]) {
                    if (j < trueSize - 1)
                        grantUsers = grantUsers + items.get(i) + ",";
                    else
                        grantUsers = grantUsers + items.get(i);
                    ++j;
                }
            }
            int selectedId = radioGroup.getCheckedRadioButtonId();

            // find the radiobutton by returned id
            radioButton =  findViewById(selectedId);
            if(radioButton.getText().toString().equals("Public"))
                flag="0";
            else
                flag="1";
            verifyToken();
        });
        verifyTokenStart();
    }

    /**
     * No PARAMS
     * This method is used to get user list from server
     * This method uses POST request to get the JSON object from the server
     */

    private void getUserList() {
        /*if ((!ConnectionManager.isNetworkAvailable(parentActivity)) || (!ConnectionManager.hasInternetAccess(parentActivity)))
            return;*/
        //hideKeyboard(parentActivity);
        StringRequest getUsersList = new StringRequest(Request.Method.POST, ServerURL.LIST_USER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("ListUsersResponse", response);
                try {
                    JSONObject checkSuccess = new JSONObject(response);
                    int success = checkSuccess.getInt("success");
                    if (response != null && success == 1) {
                        users = checkSuccess.getString("users");
                        usernames= checkSuccess.getString("usernames");
                        users=users.substring(1, users.length()-1);
                        items = Arrays.asList(users.split("\\s*,\\s*"));
                        usernames=usernames.substring(1, usernames.length()-1);
                        itemsUserName = Arrays.asList(usernames.split("\\s*,\\s*"));
                        int itemSize=items.size();
                        for(int i=0;i<itemSize;i++){
                            itemsUserName.set(i,itemsUserName.get(i).substring(1, itemsUserName.get(i).length()-1));
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter <>(CommunityActivity.this, android.R.layout.simple_list_item_multiple_choice, itemsUserName);
                        checked=new boolean[itemSize];
                        for(int i=0;i<itemSize;i++){
                            checked[i]=true;
                        }
                        multiSelectSpinner
                                .setListAdapter(adapter)
                                .setListener(new MultiSelectSpinner.MultiSpinnerListener() {
                                    @Override
                                    public void onItemsSelected(boolean[] selected) {
                                        checked=selected;
                                    }
                                })
                                .setAllCheckedText("All Students")
                                .setAllUncheckedText("No Student Selected")
                                .setSelectAll(true)
                                .setTitle("Select Students")
                                .setMinSelectedItems(2)
                                .setChoiceDialogTheme(R.style.AlertDialogCustom);
                        multiSelectSpinner.setTitleDividerColor(getResources().getColor(R.color.white));
                        //Toast.makeText(CommunityActivity.this, users, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    Log.e("CommentsResponse", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("CommentsErrorResponse", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                LoginCookies loginCookies = SharedPrefManager.getInstance(CommunityActivity.this).getLogin();
                params.put("X-CSRF-Token", loginCookies.getCsrfAccessCookie());
                params.put("Cookie", LoginCookies.JWT_ACCESS_COOKIE_NAME + "=" + loginCookies.getAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_COOKIE_NAME + "=" + loginCookies.getRefreshCookie() +
                        "; " + LoginCookies.JWT_ACCESS_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfRefreshCookie());
                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("community",  String.valueOf(NavigationActivity.currentCommunityID));
                return params;
            }
        };
        requestQueue.add(getUsersList);
    }

    /**
     * This method  send data to the server to create a community
     * This method also receives data from server once the community is created
     */

    private void getData() {
        if ((!ConnectionManager.isNetworkAvailable(this)) || (!ConnectionManager.hasInternetAccess(this)))
            return;
        StringRequest addCommunity = new StringRequest(Request.Method.POST, ServerURL.ADD_COMMUNITY, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("CommUploadResponse", response);
                try {
                    JSONObject checkSuccess = new JSONObject(response);
                    int success = checkSuccess.getInt("success");
                    if (success == 1) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(CommunityActivity.this, R.style.DialogTheme);
                        builder.setTitle("Community Created")
                                .setMessage("This community is created")
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        check=true;
                                        startActivity(new Intent(getApplicationContext(), NavigationActivity.class));
                                        finish();
                                    }
                                })
                                .setIcon(R.drawable.ic_check_black_24dp)
                                .setOnDismissListener(new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialog) {
                                        if(!check) {
                                            startActivity(new Intent(getApplicationContext(), NavigationActivity.class));
                                            finish();
                                        }
                                    }
                                })
                                .show();
                    } else
                        Toast.makeText(getApplicationContext(), "Cannot create community here!", Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    Log.e("CommUploadResponse4644", e.toString());
                }
            }
        }, error -> Log.e("CommUploadResponse", error.toString())) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                LoginCookies loginCookies = SharedPrefManager.getInstance(getApplicationContext()).getLogin();
                params.put("X-CSRF-Token", loginCookies.getCsrfAccessCookie());
                params.put("Cookie", LoginCookies.JWT_ACCESS_COOKIE_NAME + "=" + loginCookies.getAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_COOKIE_NAME + "=" + loginCookies.getRefreshCookie() +
                        "; " + LoginCookies.JWT_ACCESS_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfRefreshCookie());
                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("parent", String.valueOf(NavigationActivity.currentCommunityID) );
                params.put("name", name);
                params.put("private", flag);
                params.put("users", grantUsers);
                Log.e("xD",grantUsers);
                grantUsers="";
                return params;
            }
        };

        requestQueue.add(addCommunity);
    }

    /**
     * This is an Overridden method called by the activity when back button is pressed
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(getApplicationContext(), NavigationActivity.class));
        finish();
    }
    private void verifyTokenStart() {
        if ((!ConnectionManager.isNetworkAvailable(this)) || (!ConnectionManager.hasInternetAccess(this)))
            return;
        StringRequest verify = new StringRequest(Request.Method.POST, ServerURL.VERIFY_TOKEN_URL,
                response -> {
                    try {
                        JSONObject responseObj = new JSONObject(response);
                        if (responseObj.getInt("success") != 1) refreshTokenStart();
                        else getUserList();
                    } catch (JSONException e) {
                        refreshTokenStart();
                    }
                },
                error -> {
                    //Do stuff
                    //Probably means 401 - so try refresh
                    refreshTokenStart();
                }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                LoginCookies loginCookies = SharedPrefManager.getInstance(getApplicationContext()).getLogin();
                params.put("X-CSRF-Token", loginCookies.getCsrfAccessCookie());
                params.put("Cookie", LoginCookies.JWT_ACCESS_COOKIE_NAME + "=" + loginCookies.getAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_COOKIE_NAME + "=" + loginCookies.getRefreshCookie() +
                        "; " + LoginCookies.JWT_ACCESS_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfRefreshCookie());
                return params;
            }
        };
        requestQueue.add(verify);
    }

    /**
     * This method refreshes the token when the user list is loaded.
     * Called from onCreate method
     */

    private void refreshTokenStart() {
        final CookieManager manager = new CookieManager(null, CookiePolicy.ACCEPT_ALL);
        CookieHandler.setDefault(manager);
        StringRequest refresh = new StringRequest(Request.Method.POST, ServerURL.REFRESH_TOKEN_URL,
                response -> {
                    try {
                        JSONObject responseObj = new JSONObject(response);
                        if (!Objects.equals(responseObj.getString("refresh"), "true")) {
                            Toast.makeText(getApplicationContext(), "Please logout and login again", Toast.LENGTH_SHORT).show();
                        } else {
                            List<HttpCookie> cookieList = manager.getCookieStore().getCookies();
                            boolean accessCookieFound = false;
                            boolean accessCsrfFound = false;
                            Map<String, String> cookies = new HashMap<>();
                            for (HttpCookie cookie : cookieList) {
                                if (cookie.getName().equals(LoginCookies.JWT_ACCESS_COOKIE_NAME)) {
                                    cookies.put(LoginCookies.JWT_ACCESS_COOKIE_NAME, cookie.getValue());
                                    accessCookieFound = true;
                                }
                                if (cookie.getName().equals(LoginCookies.JWT_ACCESS_CSRF_COOKIE_NAME)) {
                                    cookies.put(LoginCookies.JWT_ACCESS_CSRF_COOKIE_NAME, cookie.getValue());
                                    accessCsrfFound = true;
                                }
                                if (accessCookieFound && accessCsrfFound) {
                                    SharedPrefManager.getInstance(getApplicationContext()).setAccessCookie(cookies);
                                    break;
                                }
                            }

                            if (accessCookieFound && accessCsrfFound)
                                getUserList();
                            else
                                Toast.makeText(getApplicationContext(), "Please logout and login again", Toast.LENGTH_SHORT).show();
                        }


                    } catch (JSONException | NullPointerException e) {
                        Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Do stuff
                        //this means that your refresh token has expired!
                        Toast.makeText(getApplicationContext(), "Please logout and login again", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                LoginCookies loginCookies = SharedPrefManager.getInstance(getApplicationContext()).getLogin();
                params.put("X-CSRF-Token", loginCookies.getCsrfRefreshCookie());
                params.put("Cookie", LoginCookies.JWT_ACCESS_COOKIE_NAME + "=" + loginCookies.getAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_COOKIE_NAME + "=" + loginCookies.getRefreshCookie() +
                        "; " + LoginCookies.JWT_ACCESS_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfRefreshCookie());
                return params;
            }
        };
        requestQueue.add(refresh);
    }

    /**
     * This method is to make sure that the token is not expired
     * This method is called before community is created
     * If it verifies the token it calls getData else it calls refreshTokenStart method
     */
    private void verifyToken() {
        if ((!ConnectionManager.isNetworkAvailable(this)) || (!ConnectionManager.hasInternetAccess(this)))
            return;
        StringRequest verify = new StringRequest(Request.Method.POST, ServerURL.VERIFY_TOKEN_URL,
                response -> {
                    try {
                        JSONObject responseObj = new JSONObject(response);
                        if (responseObj.getInt("success") != 1) refreshTokenStart();
                        else getData();
                    } catch (JSONException e) {
                        refreshTokenStart();
                    }
                },
                error -> {
                    //Do stuff
                    //Probably means 401 - so try refresh
                    refreshToken();
                }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                LoginCookies loginCookies = SharedPrefManager.getInstance(getApplicationContext()).getLogin();
                params.put("X-CSRF-Token", loginCookies.getCsrfAccessCookie());
                params.put("Cookie", LoginCookies.JWT_ACCESS_COOKIE_NAME + "=" + loginCookies.getAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_COOKIE_NAME + "=" + loginCookies.getRefreshCookie() +
                        "; " + LoginCookies.JWT_ACCESS_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfRefreshCookie());
                return params;
            }
        };
        requestQueue.add(verify);
    }
    /**
     * This method refreshes the token before the community is created.
     * Called from verifyToken
     */

    private void refreshToken() {
        final CookieManager manager = new CookieManager(null, CookiePolicy.ACCEPT_ALL);
        CookieHandler.setDefault(manager);
        StringRequest refresh = new StringRequest(Request.Method.POST, ServerURL.REFRESH_TOKEN_URL,
                response -> {
                    try {
                        JSONObject responseObj = new JSONObject(response);
                        if (!Objects.equals(responseObj.getString("refresh"), "true")) {
                            Toast.makeText(getApplicationContext(), "Please logout and login again", Toast.LENGTH_SHORT).show();
                        } else {
                            List<HttpCookie> cookieList = manager.getCookieStore().getCookies();
                            boolean accessCookieFound = false;
                            boolean accessCsrfFound = false;
                            Map<String, String> cookies = new HashMap<>();
                            for (HttpCookie cookie : cookieList) {
                                if (cookie.getName().equals(LoginCookies.JWT_ACCESS_COOKIE_NAME)) {
                                    cookies.put(LoginCookies.JWT_ACCESS_COOKIE_NAME, cookie.getValue());
                                    accessCookieFound = true;
                                }
                                if (cookie.getName().equals(LoginCookies.JWT_ACCESS_CSRF_COOKIE_NAME)) {
                                    cookies.put(LoginCookies.JWT_ACCESS_CSRF_COOKIE_NAME, cookie.getValue());
                                    accessCsrfFound = true;
                                }
                                if (accessCookieFound && accessCsrfFound) {
                                    SharedPrefManager.getInstance(getApplicationContext()).setAccessCookie(cookies);
                                    break;
                                }
                            }

                            if (accessCookieFound && accessCsrfFound)
                                getData();
                            else
                                Toast.makeText(getApplicationContext(), "Please logout and login again", Toast.LENGTH_SHORT).show();
                        }


                    } catch (JSONException | NullPointerException e) {
                        Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
                    }
                },
                error -> {
                    //Do stuff
                    //this means that your refresh token has expired!
                    Toast.makeText(getApplicationContext(), "Please logout and login again", Toast.LENGTH_SHORT).show();
                }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                LoginCookies loginCookies = SharedPrefManager.getInstance(getApplicationContext()).getLogin();
                params.put("X-CSRF-Token", loginCookies.getCsrfRefreshCookie());
                params.put("Cookie", LoginCookies.JWT_ACCESS_COOKIE_NAME + "=" + loginCookies.getAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_COOKIE_NAME + "=" + loginCookies.getRefreshCookie() +
                        "; " + LoginCookies.JWT_ACCESS_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfRefreshCookie());
                return params;
            }
        };
        requestQueue.add(refresh);
    }
}
