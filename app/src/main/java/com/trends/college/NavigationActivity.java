package com.trends.college;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.firebase.jobdispatcher.Constraint;
import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.firebase.jobdispatcher.Lifetime;
import com.firebase.jobdispatcher.RetryStrategy;
import com.firebase.jobdispatcher.Trigger;
import com.sa90.materialarcmenu.ArcMenu;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * Default activity for logged-in session
 */
public class NavigationActivity extends AppCompatActivity {
    private static final String NOTIF_SERVICE = "notif_service";
    private static final int PICKFILE_REQUEST_CODE = 8777;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    static volatile RequestQueue requestQueue;
    volatile static int currentCommunityID = -1;
    volatile static String currentCommunityName = "Institute of Engineering & Management";
    volatile static ArrayList<Object> searchResult;
    volatile static Stack<Integer> communityNavID = new Stack<>();
    volatile static Stack<String> communityNavName = new Stack<>();
    static FirebaseJobDispatcher dispatcher;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private String csv;
    //request to terminate server side session data on client logout
    private StringRequest logoutRequest = new StringRequest(Request.Method.POST, ServerURL.LOGOUT_URL,
            response -> Log.e("Logout", response), error -> Log.e("Logout", error.toString())) {
        @Override
        public Map<String, String> getHeaders() {
            Map<String, String> params = new HashMap<>();
            LoginCookies loginCookies = SharedPrefManager.getInstance(getApplicationContext()).getLogin();
            params.put("X-CSRF-Token", loginCookies.getCsrfAccessCookie());
            params.put("Cookie", LoginCookies.JWT_ACCESS_COOKIE_NAME + "=" + loginCookies.getAccessCookie() +
                    "; " + LoginCookies.JWT_REFRESH_COOKIE_NAME + "=" + loginCookies.getRefreshCookie() +
                    "; " + LoginCookies.JWT_ACCESS_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfAccessCookie() +
                    "; " + LoginCookies.JWT_REFRESH_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfRefreshCookie());
            return params;
        }
    };

    /**
     * Checks if the app has permission to write to device storage
     * <p>
     * If the app does not has permission then the user will be prompted to grant permissions
     *
     * @param activity Activity to perform check for
     */
    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

    /**
     * This method is used to get the file path of the selected file
     *
     * @param context Activity where context is generated
     * @param uri     Path returned
     * @return Environment aware absolute path
     */
    @SuppressLint("NewApi")
    public static String getFilePath(Context context, Uri uri) {
        String selection = null;
        String[] selectionArgs = null;
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        if (isExternalStorageDocument(uri)) {
            final String docId = DocumentsContract.getDocumentId(uri);
            final String[] split = docId.split(":");
            return Environment.getExternalStorageDirectory() + "/" + split[1];
        } else if (isDownloadsDocument(uri)) {
            final String id = DocumentsContract.getDocumentId(uri);
            uri = ContentUris.withAppendedId(
                    Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
        } else if (isMediaDocument(uri)) {
            final String docId = DocumentsContract.getDocumentId(uri);
            final String[] split = docId.split(":");
            final String type = split[0];
            if ("image".equals(type)) {
                uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
            } else if ("video".equals(type)) {
                uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
            } else if ("audio".equals(type)) {
                uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
            }
            selection = "_id=?";
            selectionArgs = new String[]{
                    split[1]
            };
        }
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {
                    MediaStore.Images.Media.DATA
            };
            Cursor cursor = null;
            try {
                cursor = context.getContentResolver()
                        .query(uri, projection, selection, selectionArgs, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * Default onCreate() overriden
     *
     * @param savedInstanceState Modify to store persistently
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);//initializing IDs
        if (!SharedPrefManager.getInstance(this).isLoggedIn()) {//checking shared prefs and closing default activity if needed
            Log.e("Status", "Logged Out");
            finish();
            startActivity(new Intent(this, LoginActivity.class));
            return;
        }
        verifyStoragePermissions(this);
        setContentView(R.layout.activity_navigation);
        AppBarLayout appBar = findViewById(R.id.appBarLayout);
        appBar.setElevation(0);
        appBar.bringToFront();
        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView title = findViewById(R.id.toolbar_title1);
        ArcMenu arcMenuFaculty = findViewById(R.id.arcMenu_faculty);
        ArcMenu arcMenuAdmin = findViewById(R.id.arcMenu_admin);
        requestQueue = Volley.newRequestQueue(this);
        User user;
        if (SharedPrefManager.getInstance(this).checkAccountType() == 0) {//set visibility options based on account type
            user = SharedPrefManager.getInstance(this).getStudent();
            arcMenuAdmin.setVisibility(View.GONE);
            arcMenuFaculty.setVisibility(View.GONE);
        } else if (SharedPrefManager.getInstance(this).checkAccountType() == 1) {
            user = SharedPrefManager.getInstance(this).getFaculty();
            arcMenuAdmin.setVisibility(View.GONE);

        } else {
            user = SharedPrefManager.getInstance(this).getFaculty();
            arcMenuFaculty.setVisibility(View.GONE);

        }
        title.setText(String.format("%s %s", user.getfName(), user.getlName()));
        toolbar.setTitle("");
        toolbar.setElevation(0);
        setSupportActionBar(toolbar);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, new ListFragment()).commit();
        FloatingActionButton fabCommunityAdmin = findViewById(R.id.fab_community_admin);
        fabCommunityAdmin.setOnClickListener(view -> {//adding listeners
            finish();
            Intent intent = new Intent(getApplicationContext(), CommunityActivity.class);
            startActivity(intent);
        });
        FloatingActionButton fabAddAdmin = findViewById(R.id.fab_add_admin);
        fabAddAdmin.setOnClickListener(view -> {
            finish();
            Intent intent = new Intent(getApplicationContext(), AddUserActivity.class);
            startActivity(intent);
        });
        FloatingActionButton fabRemoveAdmin = findViewById(R.id.fab_remove_admin);
        fabRemoveAdmin.setOnClickListener(view -> {
            finish();
            Intent intent = new Intent(getApplicationContext(), RemoveUserActivity.class);
            startActivity(intent);
        });
        FloatingActionButton fabMaterialFaculty = findViewById(R.id.fab_material_faculty);
        fabMaterialFaculty.setOnClickListener(view -> {
            finish();
            Intent intent = new Intent(getApplicationContext(), MaterialUploadActivity.class);
            startActivity(intent);
        });
        FloatingActionButton fabScheduleFaculty = findViewById(R.id.fab_schedule_faculty);
        fabScheduleFaculty.setOnClickListener(view -> {
            finish();
            Intent intent = new Intent(getApplicationContext(), ScheduleUploadActivity.class);
            startActivity(intent);
        });
        //currentCommunityID = -1;
        //currentCommunityName = "IEM";

        dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(getApplicationContext()));//initializing FCM based JobScheduler API
        Job notifJob = dispatcher.newJobBuilder()
                .setService(NotificationService.class) // the JobService that will be called
                .setTag(NOTIF_SERVICE)        // uniquely identifies the job
                .setRecurring(true)
                .setTrigger(Trigger.executionWindow(15, 30))
                .setReplaceCurrent(false)
                .setLifetime(Lifetime.FOREVER)
                .setRetryStrategy(RetryStrategy.DEFAULT_EXPONENTIAL)
                .setConstraints(Constraint.ON_ANY_NETWORK) //constraints which must be satisfied to run job
                .build();
        dispatcher.mustSchedule(notifJob);
    }

    /**
     * Logs out of the current app session
     */
    private void logout() {//logout feature
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this, R.style.DialogTheme);
        builder1.setMessage("Are you sure you want to logout?");
        builder1.setCancelable(true);
        builder1.setPositiveButton("Yes", (dialog, id) -> {
            requestQueue.add(logoutRequest);//server request
            currentCommunityID = -1;
            currentCommunityName = "IEM";
            dispatcher.cancel(NOTIF_SERVICE);//cancels all background listeners
            finish();
            SharedPrefManager.getInstance(getApplicationContext()).logout();//destroy persistent data
        });
        builder1.setNegativeButton("No", (dialog, id) -> dialog.cancel());
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    /**
     * Opens previous loaded ListFragment. If none present, exits app
     */
    @Override
    public void onBackPressed() {//custom override for back press action
        if (NavigationActivity.currentCommunityID != -1) {
            NavigationActivity.currentCommunityID = NavigationActivity.communityNavID.pop();
            NavigationActivity.currentCommunityName = NavigationActivity.communityNavName.pop();
            getSupportFragmentManager()
                    .beginTransaction().replace(R.id.content_frame, new ListFragment()).commit();
        } else {
            finish();
            super.onBackPressed();
        }
    }

    /**
     * Custom override for menu items select action
     *
     * @param item Current selected item
     * @return true if successful
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.logout) logout();
        if (id == R.id.logout_admin) logout();
        if (id == R.id.add_user) {
            startActivity(new Intent(this, RegisterActivity.class));
            finish();
        }
        if (id == R.id.upload_csv) {
            readCSVFile();
        }
        if (id == R.id.search) {
            SearchView searchView = (SearchView) item.getActionView();
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                /**
                 * Called when search view clicked
                 *
                 * @param query String in search box
                 * @return false by default
                 */
                @Override
                public boolean onQueryTextSubmit(String query) {
                    search(query);
                    return false;
                }

                /**
                 * Called when user types any character
                 * @param newText Changed text entered by user
                 * @return false by default
                 */
                @Override
                public boolean onQueryTextChange(String newText) {
                    return false;
                }
            });
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Called when user clicks on search
     *
     * @param query Search query submitted by user
     */
    private void search(String query) {
        NavigationActivity.searchResult = new ArrayList<>();
        for (int i = 0; i < ListFragment.currentNotes.size(); i++) {
            Notes notes = ListFragment.currentNotes.get(i);
            String tags[] = notes.getTags();
            if (notes.getTitle().toLowerCase().contains(query.toLowerCase()) || notes.getContent().toLowerCase().contains(query.toLowerCase()))
                NavigationActivity.searchResult.add(notes);
            for (String tag : tags) {
                if (tag.toLowerCase().contains(query.toLowerCase()) && (!NavigationActivity.searchResult.contains(notes))) {
                    NavigationActivity.searchResult.add(notes);
                    break;
                }
            }
        }
        for (int i = 0; i < ListFragment.currentSchedule.size(); i++) {
            Schedule schedule = ListFragment.currentSchedule.get(i);
            if (schedule.getTitle().toLowerCase().contains(query.toLowerCase()) || schedule.getContent().toLowerCase().contains(query.toLowerCase()))
                NavigationActivity.searchResult.add(schedule);
        }
        if (!NavigationActivity.searchResult.isEmpty()) {
            NavigationActivity.communityNavID.push(currentCommunityID);
            NavigationActivity.communityNavName.push(currentCommunityName);
            getSupportFragmentManager()
                    .beginTransaction().replace(R.id.content_frame, new SearchResultFragment()).commit();
        } else Toast.makeText(this, "No Results", Toast.LENGTH_SHORT).show();
    }

    /**
     * @param requestCode Specified int value to distinguish different results
     * @param resultCode  Determines if result is successful or not
     * @param data        Intent returned by the system on success
     */
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case PICKFILE_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    // Get the Uri of the selected file
                    Uri uri = data.getData();
                    csv = getFilePath(this, uri);
                    sendData();
                }
                break;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Parses CSV file from storage and sends to server in an HTTP request
     */
    private void readCSVFile() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("file/*");
        startActivityForResult(intent, PICKFILE_REQUEST_CODE);
    }

    private void sendData() {
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        BufferedReader in;
        File csvFile;
        csvFile = new File(csv);
        try {
            in = new BufferedReader(new FileReader(csvFile));
            while ((line = in.readLine()) != null) stringBuilder.append(line).append("\n");
        } catch (Exception e) {
            Log.e("Exception", e.toString());
        }

        String csvData = stringBuilder.toString();
        StringRequest uploadCSV = new StringRequest(Request.Method.POST, ServerURL.UPLOAD_CSV, response -> {
            Log.e("UploadResponse", response);
            try {
                JSONObject checkSuccess = new JSONObject(response);
                int success = checkSuccess.getInt("response");
                if (success == 1) {
                    Toast.makeText(getApplicationContext(), "CSV upload successful", Toast.LENGTH_SHORT).show();
                } else
                    Toast.makeText(NavigationActivity.this, "Please upload a correct file.", Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                Log.e("UploadResponse", e.toString());
            }
        }, error -> Log.e("UploadErrorResponse", error.toString())) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                LoginCookies loginCookies = SharedPrefManager.getInstance(getApplicationContext()).getLogin();
                params.put("X-CSRF-Token", loginCookies.getCsrfAccessCookie());
                params.put("Cookie", LoginCookies.JWT_ACCESS_COOKIE_NAME + "=" + loginCookies.getAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_COOKIE_NAME + "=" + loginCookies.getRefreshCookie() +
                        "; " + LoginCookies.JWT_ACCESS_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfAccessCookie() +
                        "; " + LoginCookies.JWT_REFRESH_CSRF_COOKIE_NAME + "=" + loginCookies.getCsrfRefreshCookie());
                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("csv", csvData);
                return params;
            }
        };
        uploadCSV.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(uploadCSV);

        /*if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, LoginActivity.WRITE_REQUEST);*/
    }

    /**
     * Inflate the menu; this adds items to the action bar if it is present.
     *
     * @param menu Current menu ID from related XML
     * @return true by default if inflation successful
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {//inflate menu items based on current theme and screen size/orientation
        if (SharedPrefManager.getInstance(this).checkAccountType() == 2)
            getMenuInflater().inflate(R.menu.navigation, menu);
        else
            getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
}