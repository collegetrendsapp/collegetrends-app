package com.trends.college;

import org.junit.Test;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void checkServerURL() {
        InetAddress address = null;
        try {
            address = InetAddress.getByName(ServerURL.getBaseUrl());
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        try {
            assert address != null;
            assertEquals(address.isReachable(4), true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}