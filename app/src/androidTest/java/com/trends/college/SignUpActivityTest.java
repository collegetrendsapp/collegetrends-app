package com.trends.college;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.test.ActivityInstrumentationTestCase2;
import android.test.UiThreadTest;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
public class SignUpActivityTest extends ActivityInstrumentationTestCase2<SignUpActivity> {

    private String name;
    private String email;
    private String year;
    private String ph;
    private String pass;
    private String repass;
    private EditText nameView;
    private EditText emailView;
    private EditText phoneView;
    private EditText yearView;
    private EditText passView;
    private EditText repassView;
    private Spinner account_type;
    private Spinner streamSpin;
    private SignUpActivity activity;
    private Button register;
    private TextView login;

    public SignUpActivityTest() {
        super(SignUpActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        activity = getActivity();
        account_type = activity.findViewById(R.id.account_type);
        streamSpin = activity.findViewById(R.id.stream);
        nameView = activity.findViewById(R.id.input_name);
        emailView = activity.findViewById(R.id.input_email);
        phoneView = activity.findViewById(R.id.input_phone);
        yearView = activity.findViewById(R.id.input_year);
        passView = activity.findViewById(R.id.input_password);
        repassView = activity.findViewById(R.id.input_reEnterPassword);
        register = activity.findViewById(R.id.btn_signup);
        login = activity.findViewById(R.id.link_login);
    }

    @Test
    public void testPreconditions() {
        assertNotNull("activity is null", activity);
        assertNotNull("phone EditText is null", phoneView);
        assertNotNull("Name EditText is null", nameView);
        assertNotNull("Email EditText is null", emailView);
        assertNotNull("Year EditText is null", yearView);
        assertNotNull("password EditText is null", passView);
        assertNotNull("RE enter password EditText is null", repassView);
        assertNotNull("Account Spinner is null", account_type);
        assertNotNull("Stream Spinner is null", streamSpin);
        assertNotNull("Register button is null", register);
        assertNotNull("Login TextView is null", login);
    }

    @UiThreadTest
    public void testDataEntered() {
        ph = phoneView.getText().toString();
        name = nameView.getText().toString();
        email = emailView.getText().toString();
        year = yearView.getText().toString();
        pass = passView.getText().toString();
        repass = repassView.getText().toString();
        assertEquals("Not 10 digit no.", ph.length(), 10);
        boolean b;
        try {
            Long.parseLong(ph);
            b = true;
        } catch (NumberFormatException ex) {
            Log.e("Parse", ex.toString());
            b = false;
        }
        assertEquals("Contains non numeric characters in phone number", b, true);
        assertEquals("Password has <6 characters", pass.length() >= 6, true);
        assertEquals("Name is empty", name.length() != 0, true);
        assertEquals("Year is invalid", year.length() == 1 && (Character.isDigit(year.charAt(0))), true);
        assertEquals("Passwords do not match", repass.length() != 0 && repass.equals(pass), true);
    }

    @Test
    public void isNetworkAvailable() {
        assertEquals("Network not available", ConnectionManager.isNetworkAvailable(activity), true);
    }

    @Test
    public void isInternetAvailable() {
        assertEquals("Internet connection not available", ConnectionManager.hasInternetAccess(activity), true);
    }

    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();
        assertEquals("com.trends.college", appContext.getPackageName());
    }
}