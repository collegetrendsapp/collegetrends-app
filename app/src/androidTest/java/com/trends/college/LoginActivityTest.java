package com.trends.college;

import android.content.Context;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.test.ActivityInstrumentationTestCase2;
import android.test.UiThreadTest;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
public class LoginActivityTest extends ActivityInstrumentationTestCase2<LoginActivity> {

    private LoginActivity activity;
    private EditText phone;
    private EditText password;
    private Spinner accType;
    private Button login;
    private TextView signUp;

    public LoginActivityTest() {
        super("com.trends.college", LoginActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        // Starts the activity under test using
        // the default Intent with:
        // action = {@link Intent#ACTION_MAIN}
        // flags = {@link Intent#FLAG_ACTIVITY_NEW_TASK}
        // All other fields are null or empty.
        Log.e("Setup", "Called");
        //this.startActivity(new Intent(), null, null);
        activity.startActivity(new Intent(), null);
        activity = this.getActivity();
        phone = activity.findViewById(R.id.input_phone);
        password = activity.findViewById(R.id.input_password);
        accType = activity.findViewById(R.id.account_type);
        login = activity.findViewById(R.id.btn_login);
        signUp = activity.findViewById(R.id.link_signup);
    }

    @Test
    public void testPreconditions() {
        assertNotNull("activity is null", getActivity());
        assertNotNull("phone edittext is null", phone);
        assertNotNull("password edittext is null", password);
        assertNotNull("Spinner is null", accType);
        assertNotNull("Login button is null", login);
        assertNotNull("signup TextView is null", signUp);
    }

    @UiThreadTest
    public void testDataEntered() {
        String ph = phone.getText().toString();
        assertEquals("Not 10 digit no.", ph.length(), 10);
        boolean b;
        try {
            Long.parseLong(ph);
            b = true;
        } catch (NumberFormatException ex) {
            Log.e("Parse", ex.toString());
            b = false;
        }
        assertEquals("Contains non numeric characters in phone number", b, true);
        assertEquals("Password has <6 characters", password.getText().toString().length() >= 6, true);
    }

    /*@SmallTest
    public void testViews() {
        assertNotNull(getActivity());
        assertNotNull(mTextView);
        assertNotNull(mCKilos);
        assertNotNull(mCPounds);
        assertNotNull(mKilos);
        assertNotNull(mPounds);
    }*/

    @Override
    protected void tearDown() throws Exception {
        //close all resources over here
        super.tearDown();
    }

    @Test
    public void isNetworkAvailable() {
        assertEquals("Network not available", ConnectionManager.isNetworkAvailable(activity), true);
    }

    @Test
    public void isInternetAvailable() {
        assertEquals("Internet connection not available", ConnectionManager.hasInternetAccess(activity), true);
    }

    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();
        assertEquals("com.trends.college", appContext.getPackageName());
    }
}